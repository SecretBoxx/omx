// =================================================================
//    OMX: receiving and storage of remote vessels' backup files
// 
// (c) 11/2022-2024+ Boris Segret, due to OMP, GPLv2 license applies
// ORBITER (C) 2003-2016 Martin Schweiger, under MIT license
// OMX is an evolution of OMP (Orbiter Multiplayer Project), great
// work by Friedrich Kastner - Masilko, (C) 2007 under GPL.
// =================================================================
//
// OMX_backupServer.exe is launched by ServerConsole.Main:
// - receives vessel's backup files in TCP, checks for consistency, then stores as "9[callsign].data"
// - keeps running during [countdown] seconds after ServerConsole process stopped (for any reason)
// - keeps reconnecting as a listening server in case of disconnection
// It listens for files in a non-blocking mode using the select() command. Storage path is 
// ..\Data\.

#include <iostream>
//#include <pqxx/pqxx> // for Databases
#include <ctime>
#include <string>
#include <fstream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <psapi.h>
#include "..\OMPClient\STC_BNR.h"

#pragma comment(lib, "ws2_32.lib")

std::string mystr;
#define _RETURN std::getline(std::cin, mystr); return

int main(int argc, char* argv[]);
int createAndBindSocket(addrinfo * servinfo);
int storeBackupStatus(SOCKET sock);
void logThis(const std::string & msg, bool checkforDuplicate = false) {
    // "true" means "to server"
    const bool toServer = true;
    logThis(toServer, msg, checkforDuplicate);
}

/// <summary>
/// Watches for active ServerConsole process, starts a countdown (e.g. 10s) if it disappears, then exits. 
/// Creates a listeningSocket and sets it to a non-blocking mode, to receive incoming TCP. For each new 
/// client requesting a connect, a new clientSocket is created and added to the non-blocking monitoring. 
/// If activity is detected on a clientSocket, storeBackupStatus is called. If an error occurs, the 
/// clientSocket or all sockets are closed and a new socket creation process starts.
/// </summary>
/// <param name="argc">Nb of arguments passed by ServerConsole.Main, must be 4</param>
/// <param name="argv">expected commandline is [this.exe] [pid] [port] [IP], with pid = ServerConsole's 
/// process ip, port = listening port (game's port +1), IP = server's public IP. If no arguments are 
/// passed, default values are taken (i.e. debug session).</param>
/// <returns></returns>
int main(int argc, char* argv[]) {
    std::string logmsg;
    WSADATA wsaData;
    SOCKET listeningSocket, clientSocket;
    struct sockaddr_in serverAddr, clientAddr;
    struct addrinfo* result = NULL, hints;
    int addrlen = sizeof(clientAddr);
    fd_set masterSet, readSet;
    int max_sd;
    u_long nonBlockingMode;

    DWORD sleepTime = 1000; // [ms]
    const struct timeval timeout = { 0, 500000 };  // {[second], [microsec]}
    bool WSAStartup_flag = false;
    bool Socket_creation_flag = false;
    bool Socket_listening_flag = false;
    int rv;
    bool stopsoon = false; // final watchdog before exiting
    double countdown = 10.; // [s]
    time_t stop = 0, now;

    HANDLE hProcess;
    struct srvData {
        char publicIP[MAX_PATH];
        char strport[10];
        bool active;
        DWORD pid;
        u_short port;
        wchar_t name[MAX_PATH];
    } srv;
    DWORD pathLength;

    logmsg = ":\n=================== Backup Server Daemon";
    // setting up params transfered by ServerConsole.exe
    srv.active = true;
    if (argc != 4) {
        srv.pid = GetCurrentProcessId(); // if 0 was sent, we're in DEBUG, so we take our own PID
        srv.port = 1516;
        strcpy(srv.strport, "1516");
        strcpy(srv.publicIP,"10.242.54.254"); // or from https://api.ipify.org/ 
    }
    else {
        srv.pid  = (DWORD)atol(argv[1]); // if 0 was sent, we're in DEBUG, so we take own PID
        srv.port = (u_short)atol(argv[2]);
        sprintf(srv.strport, "%d", srv.port);
        strcpy(srv.publicIP, argv[3]);
    }
    logmsg.append(", PID " + std::to_string(srv.pid));
    logmsg.append("\n                    Server "
        + std::string(srv.publicIP) + ":" + std::to_string(srv.port) + "\n===================");
    logThis(logmsg);

    /*std::cout << "===========================================" << std::endl;
    std::cout << "  Also Testing database feature for later"   << std::endl;
    std::cout << "===========================================" << std::endl;
    try {
        pqxx::connection cx("dbname = omx user = omxadmin password = admin hostaddr = 127.0.0.1 port = 5000");
        if (cx.is_open()) {
            std::cout << "Opened database successfully: " << cx.dbname() << std::endl;
        }
        else {
            std::cout << "Can't open database" << std::endl;
            return 1;
        }
        pqxx::work tx{ cx };
        pqxx::result rx{ tx.exec("SELECT name, gID, mass FROM celBodies;") };
        tx.commit();
        for (auto row : rx)
        {
            std::cout << "Row: ";
            // Iterate over fields in a row.
            for (auto field : row) std::cout << field.c_str() << " ";
            std::cout << std::endl;
        }
        cx.close();
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    std::cout << "===========================================" << std::endl;
    */

    while (true) {
        hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, srv.pid);
        if (hProcess != NULL) {
            DWORD exitCode;
            GetExitCodeProcess(hProcess, &exitCode);
            CloseHandle(hProcess);  // Don't forget to close the handle
            if (exitCode != STILL_ACTIVE) { hProcess = NULL; }
        }
        if (hProcess == NULL) {
            time(&now);
            // ServerConsole.exe was terminated => watchdog triggered for <countdown> seconds.
            if (!stopsoon) {
                stopsoon = true;
                time(&now); stop = now + countdown;
                logmsg = "-| ServerConsole is NO more running |";
                logmsg.append("\n                    -| ...                              |");
                logThis(logmsg);
            }
            // this process keeps running for late sending of backup files after server exited
            if (stopsoon && (now > stop)) break;
        }
        // Initialize Winsock
        if (!WSAStartup_flag) {
            if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
                logThis(" -> WSAStartup failed. Will retry after timeSleep.");
                Sleep(sleepTime);
            }
            else {
                memset(&hints, 0, sizeof hints);
                hints.ai_family = AF_INET;
                hints.ai_socktype = SOCK_STREAM;
                hints.ai_protocol = IPPROTO_TCP;
                hints.ai_flags = AI_PASSIVE;
                if ((rv = getaddrinfo(srv.publicIP, srv.strport, &hints, &result)) != 0) {
                    logThis(" -> getaddrinfo: error " + std::to_string(rv));
                    WSACleanup(); //WSAStartup_flag = false;
                    Sleep(sleepTime);
                } else {
                    logThis("WSAStartup.");
                    WSAStartup_flag = true;
                }
            }
        }
        if (WSAStartup_flag) {
            // Create socket
            if (!Socket_creation_flag) {
                int possibleSocket;
                if ((possibleSocket = createAndBindSocket(result)) == -1) {
                    logThis(" -> Listening Socket creation failed: (retrying after sleepTime): " + err_WSAGetLastError());
                    freeaddrinfo(result);
                    WSACleanup(); WSAStartup_flag = false;
                    Sleep(sleepTime);
                }
                else {
                    listeningSocket = (SOCKET)possibleSocket;
                    logmsg = "Listening Socket " + std::to_string(listeningSocket) + ": created and bound";
                    freeaddrinfo(result);
                    Socket_creation_flag = true;
                }
            }
            if (Socket_creation_flag) {
                // Set the socket to non-blocking mode
                if (!Socket_listening_flag) {
                    nonBlockingMode = 1;
                    ioctlsocket(listeningSocket, FIONBIO, &nonBlockingMode);
                    // Listen for incoming connections
                    if (listen(listeningSocket, SOMAXCONN) == SOCKET_ERROR) {
                        logmsg.append(", but listening FAILED."); logThis(" -> " + logmsg);
                        closesocket(listeningSocket); Socket_creation_flag = false;
                    }
                    else {
                        logThis(logmsg.append(", and listening on port " + std::to_string(srv.port) + "..."));
                        // Initialize the master set and add the listening socket to it
                        FD_ZERO(&masterSet);
                        FD_SET(listeningSocket, &masterSet);
                        max_sd = listeningSocket;
                        Socket_listening_flag = true;
                    }
                }
                if (Socket_listening_flag)
                {
                    // Wait for activiy on the existing sockets with select()
                    readSet = masterSet;
                    int activity = select(max_sd + 1, &readSet, NULL, NULL, &timeout); // last NULL = waits indefinitely
                    if (activity == SOCKET_ERROR) {
                        logmsg = "-   select() failed! " + err_WSAGetLastError() + ", max_sd = "; std::to_string(max_sd);
                        logThis(logmsg + ". Listening Socket closing.");
                        Socket_listening_flag = false;
                        continue;
                    }
                    else if (activity == 0) continue;
                    else {
                        if (activity > max_sd) logThis("Current opened sockets: " + std::to_string(activity));
                    }
                    if (FD_ISSET(listeningSocket, &readSet)) {
                        // Activity is on the listening port, i.e. new connection
                        clientSocket = accept(listeningSocket, NULL, NULL);
                        if (clientSocket == INVALID_SOCKET) {
                            logThis(" -> entering TCP from a new client failed! " + err_WSAGetLastError());
                            continue;
                        }
                        // Set the client socket to non-blocking mode and add it to the set to be "selectable"
                        ioctlsocket(clientSocket, FIONBIO, &nonBlockingMode); // non-blocking recv, send
                        struct sockaddr_in clientAddr;
                        socklen_t clientAddrSize = sizeof(clientAddr);
                        char ip_str[INET_ADDRSTRLEN];
                        if (getpeername(clientSocket, (struct sockaddr*)&clientAddr, &clientAddrSize) != 0) logThis("-   Reading clientAddr: error " + err_WSAGetLastError());
                        inet_ntop(AF_INET, &clientAddr.sin_addr, ip_str, sizeof(ip_str));
                        FD_SET(clientSocket, &masterSet);
                        max_sd = (max_sd < clientSocket) ? clientSocket : max_sd;
                        logmsg = "Client socket " + std::to_string(clientSocket) + " NEW, for " + std::string(ip_str) + ":" + std::to_string(ntohs(clientAddr.sin_port));
                        logThis(logmsg);
                    }
                    // Reads from all sockets (known and new clients)
                    for (int i = 0; i <= max_sd; i++) {
                        if (FD_ISSET(i, &readSet)) {
                            if (i == (int)listeningSocket) continue;
                            // Client socket is readable
                            int returnValue;
                            if ((returnValue = storeBackupStatus((SOCKET)i)) == 0) {
                                closesocket(i); // Connection closed by client, so we do
                                FD_CLR(i, &masterSet);
                            }
                            else if (returnValue < 0) {
                                // Error in receiving data
                                if (returnValue == -100) {/* debug message already managed in storeBackupStatus(..) */}
                                else {
                                    logmsg = " -> Client Socket " + std::to_string(i) + ": recv failed, then closing! " + err_WSAGetLastError();
                                    logThis(logmsg);
                                }
                                closesocket(i);
                                FD_CLR(i, &masterSet);
                            }
                        }
                    }
                }
            }
        }
    }
    logThis("-|_OMX_backupServer (PID " + std::to_string(srv.pid) +") stopped_____ | ");
    closesocket(listeningSocket);
    WSACleanup();
    return 0;
}

/// <summary>
/// As part of the non-blocking mode, the listeningSocket is searched, created, bound and returned.
/// </summary>
/// <param name="servinfo">List of compatible network connections</param>
/// <returns>New socket created and bound.</returns>
int createAndBindSocket(addrinfo* servinfo) {
    SOCKET server_fd = NULL;
    int possibleSocket = -1;
    addrinfo* p;
    for (p = servinfo; p != NULL; p = p->ai_next) {
        // Create socket
        if ((possibleSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) continue;
        // Reuse the address to avoid "address already in use" error
        server_fd = (SOCKET)possibleSocket;
        if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, "1", sizeof(int)) == -1) {
            logThis(" -> Client Socket " + std::to_string(server_fd) + ": setsockopt for re - use failed!");
            closesocket(server_fd);
            return -1;
        }
        // Bind the socket to the public IP and port
        if (bind(server_fd, p->ai_addr, p->ai_addrlen) == -1) {
            closesocket(server_fd);
            continue;
        }
        break;
    }
    if (p == NULL) {
        logThis(" -> Client Socket " + std::to_string(server_fd) + ": failed to bind!");
        closesocket(server_fd);
        return -1;
    }
    return server_fd;
}



#define PREFIX "9"          // prefix for server-processed backup files
#define NAMELENGTH 50
/// <summary>
/// Backup file is preceded by the sending of its size (< BUFFER_SIZE). Then, it is saved as a file called 
/// after the included vessel's name. More control is possible before storage (e.g. relevance of the file). 
/// The routine does not manage the socket and only returns the possible error on recv.
/// </summary>
/// <param name="sock">number of the socket to be read</param>
/// <returns>-100 or -1 if error, 0 if no more data, otherwise length received</returns>
#include <filesystem>
#include <regex>
int storeBackupStatus(SOCKET sock)
{
    std::string logmsg;
    char socketFile[256];
    char buffer[BUFFER_SIZE]; int bufferIdx = 0;
    int fileSize, currentSize, bytesRemaining;
    std::fstream bkfile;
    char vName[NAMELENGTH];
    char fName[256];
    std::regex regexPattern;
    extendedPacket exPac;
    bool consistent = true;

    // empties the socket
    int bytesReceived = recv(sock, buffer, BUFFER_SIZE, 0);
    if (bytesReceived == 0) {
         // Connection closed by client
         logThis("Client Socket " + std::to_string(sock) + " gently closed.");
         return 0;
    }
    if (bytesReceived < 0) {
        logmsg = " -> Client Socket " + std::to_string(sock) + ": error at receiving -> closed! ";
        logThis(logmsg + err_WSAGetLastError());
        return -100;
    }

    // Warning: multiple files may accumulate in the buffer -> empties the buffer
    sprintf(socketFile, "%ss_%i.data", LOC_PATH, sock);
    bytesRemaining = bytesReceived;
    while (bytesRemaining > 0) {
        // either [socketFile] = "s_[sock].data" exists and the buffer contains data to be appended to it,
        // or it doesn't and bufferIdx should point to "*NN*", where NN = length of the file to be received.
        // 
        // Below: creates socketFile or appends to it, inits fileSize and currentSize
        if (!std::filesystem::exists(socketFile)) {
            // [socketFile] doesn't exist yet => creates it, writes the header "*NN*" first,
            // then writes the rest of the buffer up to the expected fileSize.
            char a[2]; strncpy(a, &buffer[bufferIdx], 1); a[1] = 0x0;
            char b[2]; strncpy(b, &buffer[bufferIdx + FILESIZE - 1], 1); b[1] = 0x0;
            consistent  = (strncmp(a, "*", 1) == 0);
            consistent &= (strncmp(b, "*", 1) == 0);
            memcpy(&fileSize, &buffer[bufferIdx + 1], sizeof(int));
            consistent &= (fileSize <= BUFFER_SIZE - FILESIZE);
            if (!consistent) {
                logThis(" -> File " + std::string(socketFile)
                    + ": Header " + std::string(a) + std::to_string(fileSize) + std::string(b)
                    + " --> not consistent");
                return -1;
            }
            bkfile.open(socketFile, std::ios::out | std::ios::binary);
            if (!bkfile.is_open()) {
                bkfile.close();
                logThis(" -> File " + std::string(socketFile) + " couldn't be saved.");
                return -1;
            }
            bkfile.write(&buffer[bufferIdx], FILESIZE);
            bytesRemaining -= FILESIZE;
            bufferIdx += FILESIZE;
            currentSize = 0;
            // next loop (on same buffer) will append the buffer in [socketFile]
        }
        else {
            // [socketFile] exists => retreives the currentSize and expected fileSize,
            // appends the rest of the buffer to the temporary socket file up to the expected fileSize.
            bkfile.open(socketFile, std::ios::in | std::ios::binary | std::ios::app);
            if (!bkfile.is_open()) {
                bkfile.close();
                std::cerr << "File " << socketFile << " couldn't re-open -> removed!" << std::endl;
                remove(socketFile);
                return -1;
            }
            bkfile.seekg(0, std::ios::end); // positionned at the end
            currentSize = (int)bkfile.tellg() - FILESIZE;
            bkfile.seekg(1, std::ios::beg); // positionned just after the first "*"
            bkfile.read((char*)&fileSize, sizeof(int));
            std::streamsize streamLength;
            streamLength = fileSize - currentSize;
            bkfile.seekp(0, std::ios::end); // positionned at the end, to write
            if (streamLength == 0) {}
            else if (streamLength <= bytesRemaining) {
                // The buffer contains more than the missing data for the current backup file.
                // Only the expected missing data is appended.
                bkfile.write(&buffer[bufferIdx], streamLength);
                bufferIdx += streamLength;
                bytesRemaining = bytesReceived - bufferIdx;
                currentSize = fileSize; // needed for the condition to rename [socketFile]
            }
            else {
                // More data is still expected (the buffer contains less than the missing data).
                // The rest of the buffer is appended and a new recv command will be needed.
                bkfile.write(&buffer[bufferIdx], bytesRemaining);
                currentSize += bytesRemaining;
                bytesRemaining = 0;
            }
        }
        bkfile.close();

        if (currentSize == fileSize) {
            logmsg = "Client Socket " + std::to_string(sock) + ", file size: "
                + std::to_string(fileSize) + "[B]: ";
            bkfile.open(socketFile, std::ios::in | std::ios::binary);
            // consistency checks and renaming
            if (!bkfile.is_open()) {
                bkfile.close();
                logThis(" -> " + logmsg.append(std::string(socketFile) + " couldn't re-open -> removed!"));
                remove(socketFile);
                return -1;
            }
            bkfile.seekg(FILESIZE, std::ios::beg); // positionned just after the second "*"
            bkfile.read((char*)&exPac, sizeof(extendedPacket));
            // using vName for owner's name
            bkfile.read(vName, exPac.ownerNameLength); vName[exPac.ownerNameLength] = 0x0;
            regexPattern = "[A-Za-z][A-Za-z0-9 ]*[A-Za-z0-9]";
            consistent &= (std::regex_match(vName, regexPattern));
            // using vName for vessel's name
            bkfile.read(vName, exPac.vesselNameLength); vName[exPac.vesselNameLength] = 0x0;
            regexPattern = CALLSIGNPATTERN;
            consistent &= (std::regex_match(vName, regexPattern));
            sprintf(fName, "%s%s%s.data", LOC_PATH, PREFIX, vName); // inits new file name
            // using vName for class' name
            bkfile.read(vName, exPac.vClassNameLength); vName[exPac.vClassNameLength] = 0x0;
            regexPattern = "[A-Za-z][A-Za-z0-9 ]*[A-Za-z0-9]";
            consistent &= (std::regex_match(vName, regexPattern));
            bkfile.close();

            if (!consistent) {
                logThis(" -> " + logmsg.append(std::string(socketFile) + ", names not consistent -> file removed!"));
                remove(socketFile);
                return -1;
            }
            remove(fName);
            if (rename(socketFile, fName) == 0) {
                logThis(logmsg.append("renamed as " + std::string(fName)), true);
            }
            else {
                logmsg.append(std::string(socketFile) + " couldn't be renamed into " + std::string(fName) + " -> both files removed!");
                logThis(logmsg);
                remove(socketFile);
                remove(fName);
                return -1;
            }
        }
        // at this stage, the buffer may still contain data => loops until bytesRemaining == 0
    }
    return bytesReceived;
}

