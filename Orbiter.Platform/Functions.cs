using System.Runtime.InteropServices;
#if UNIX
using Mono.Unix.Native;
#endif

namespace Orbiter.Platform
{
    /// <summary>
    /// Platform-specific functions.
    /// </summary>
    public class Functions
    {
#if UNIX
        public static bool QueryPerformanceFrequency(out long frequency)
        {
        	frequency=1000000;
        	return true;
        }
        public static bool QueryPerformanceCounter(out long count)
        {
        	Timeval tv;
            Timezone tz;
            Syscall.gettimeofday(out tv, out tz);
        	count=tv.tv_sec*1000000+tv.tv_usec;
        	return true;
        }
#else
        /// <summary>
        /// Queries the performance frequency.
        /// </summary>
        /// <param name="frequency">The frequency.</param>
        /// <returns><c>True</c>, if frequency is valid; otherwise <c>False</c>.</returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool QueryPerformanceFrequency(out long frequency);
        /// <summary>
        /// Queries the performance counter.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <returns><c>True</c>, if counter is valid; otherwise <c>False</c>.</returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool QueryPerformanceCounter(out long count);
#endif
    }
}
