// =================================================================
//         OMX: Sending local backup files to the server
// 
// (c) 11/2022-2024+ Boris Segret. Due to OMP, GPLv2 license applies
// ORBITER (C) 2003-2016 Martin Schweiger, under MIT license
// OMX is an evolution of OMP (Orbiter Multiplayer Project), great
// work by Friedrich Kastner - Masilko, (C) 2007 under GPL.
// =================================================================
//
// OMX_backupClient.exe is launched by OMPClient.cpp/hostsenthread (thread). Then:
// - any file "[orbiter\]Server/Data/0[callsign].data" is TCP-sent to OMX Server, then renamed into 1[callsign].data
// - keeps reconnecting with the server in case of disconnection
// - keeps running a few seconds more if Orbiter crashes, in order to send the latest backup
// - exits, after some delay, if no more 0[callsign].data
// - loggs by appending to "[orbiter\]Server/Windows/BackupClientLog.txt"
// 

#include <iostream>
#include <string>
#include <fstream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include "..\OMPClient\STC_BNR.h"

#pragma comment(lib, "ws2_32.lib")

#define SLEEPTIME 990       // [ms], loops after this time (1 file sent at a time)
#define CLOSINGDELAY 3+2*SLEEPTIME/1000. // [s], max delay to update at least 1 local backup file

// Local prototypes:
int main(int argc, char* argv[]);           // launched by OMPClient.cpp
int readServerInfo(int argc, char* argv[]); // retrieves OMX server info, called by main
int sendOldestBackup(SOCKET* psock);        // sends 1 file on psock, called by main
void logThis(const std::string& msg, bool checkforDuplicate = false) {
    // "false" means "to client"
    const bool toServer = false;
    logThis(toServer, msg, checkforDuplicate); // def.in STC_BNR.h
}

// Global variables
time_t now, stop;
struct srvData {
    char *ipchar;
    struct sockaddr_in Addr;
    unsigned long publicIP;
    char strport[10];
    bool active;
    DWORD pid;
    u_short port;
    wchar_t name[MAX_PATH];
} srv;


/// <summary>
/// Manages the opening/re-opening of a TCP connection with OMX server: tolerant to temporary 
/// losses of connection. Calls subroutines to select and send backup files. Closes if no more 
/// files are to be sent during [CLOSINGDELAY] seconds.
/// </summary>
/// <note>Still a bug: should restart if closed for any reason</note>
int main(int argc, char* argv[]) {
    WSADATA wsaData;
    SOCKET sock;
    DWORD sleepTime = 990; // [ms]
    DWORD result;
    time(&now); stop = now + CLOSINGDELAY;

    std::string logmsg;
    logmsg = ":\n=================== PID " + std::to_string(GetCurrentProcessId()) + ". ";
    char buffer[MAX_PATH];
    result = GetCurrentDirectoryA(MAX_PATH, buffer);
    logThis(logmsg.append(std::string(buffer) + "\\" + BKP_PATH));

    bool WSAStartup_flag = false;
    bool Socket_creation_flag = false;
    bool Connection_flag = false;

    time(&now); stop = now + CLOSINGDELAY;
    while (true) {
        logmsg = "";
        if (now > stop) break;
        // Initializes Winsock
        if (!WSAStartup_flag) {
            if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
                logThis(logmsg + " -> WSAStartup failed (retrying after sleepTime): " + err_WSAGetLastError());
            } else {
                WSAStartup_flag = true;
                readServerInfo(argc, argv); // initizes srv
                logmsg = "WSAStartup -> Server "
                    + std::string(srv.ipchar) + ":" + std::to_string(srv.port);
                logThis(logmsg);
            }
        }
        if (WSAStartup_flag) {
            // Creates a socket
            if (!Socket_creation_flag) {
                if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET) {
                    logThis(" -> Socket creation failed: error " + err_WSAGetLastError());
                    WSACleanup();
                    WSAStartup_flag = false;
                }
                else {
                    Socket_creation_flag = true;
                    logmsg = "Socket " + std::to_string((int)sock) + " created. ";
                }
            }
            if (Socket_creation_flag) {
                // Connects to the server
                if (!Connection_flag) {
                    if (connect(sock, (struct sockaddr*)&srv.Addr, sizeof(srv.Addr)) == SOCKET_ERROR) {
                        logThis(" -> " + logmsg.append("Connection failed: error " + err_WSAGetLastError()));
                        closesocket(sock);
                        Socket_creation_flag = false;
                        Connection_flag = false;
                    }
                    else {
                        logThis(logmsg.append("Connection established."));
                        Connection_flag = true;
                        time(&now); stop = now + CLOSINGDELAY; // initized here, updated if sendOldestBackup successful
                    }
                }
                if (Connection_flag) {
                    // if result == 0, <stop> is postponed (a file was sent)
                    // if result == 1, no file was sent, <stop> is unchanged
                    // if result < 0, an error occured, a new socket will be created
                    if ((result = sendOldestBackup(&sock)) < 0) {
                        closesocket(sock);
                        Socket_creation_flag = false;
                        Connection_flag = false;
                    }
                    else if (result == 0) stop = now + CLOSINGDELAY;
                }
            }
        }
        time(&now);
        Sleep(sleepTime);
    }
    if (Socket_creation_flag) closesocket(sock);
    WSACleanup();
    return 0;
}

/// <summary>
/// Server's info are sent within arguments passed to main, and just transferred here.
/// If no arguments are passed, that's because main was launched for debug and default 
/// parameters are considered. Warning: if server is given with its domain name (node), 
/// it is here translated into a 4-byte IP address.
/// </summary>
/// <param name="argc"></param>
/// <param name="argv"></param>
/// <returns></returns>
int readServerInfo(int argc, char* argv[])
{
    if (argc != 3) {
        srv.port = 1516;
        strcpy(srv.ipchar, "(PCCENSUS01.ZeroTier)");
        srv.publicIP = inet_addr("10.242.54.254"); // or from https://api.ipify.org/ 
        std::memset(&srv.Addr, 0, sizeof(srv.Addr));
        srv.Addr.sin_family = AF_INET;
        srv.Addr.sin_port = htons(srv.port);
        srv.Addr.sin_addr.S_un.S_addr = srv.publicIP;
    }
    else {
        srv.ipchar = argv[1];
        srv.port = (u_short)atol(argv[2]);
        if ((srv.publicIP = inet_addr(srv.ipchar)) == INADDR_NONE) {
            const char* service = nullptr;  // Optional argument not provided
            struct addrinfo* res, * p;
            int status = getaddrinfo(srv.ipchar, service, nullptr, &res);
            if (status != 0) {
                logThis(" -> Domain name could not be translated into an IP address!");
                return 1;
            }
            // Loop through all the results and use the first we can
            for (p = res; p != nullptr; p = p->ai_next) {
                // We want an IPv4 address
                if (p->ai_family == AF_INET) {
                    // Copy the address information into srv.Addr
                    std::memset(&srv.Addr, 0, sizeof(srv.Addr));
                    srv.Addr.sin_family = AF_INET;
                    srv.Addr.sin_port = htons(srv.port);
                    srv.Addr.sin_addr = ((struct sockaddr_in*)p->ai_addr)->sin_addr;
                    break;
                }
            }
            logThis("Domain name = IP " + std::string(inet_ntoa(srv.Addr.sin_addr)));
        }
        else {
            srv.Addr.sin_family = AF_INET;
            srv.Addr.sin_port = htons(srv.port);
            srv.Addr.sin_addr.S_un.S_addr = srv.publicIP;
        }
    }
    return 0;
}


/// <summary>
/// Sends only the oldest 0[callsign].data backup file to the server in one single send command.
/// Then, renames with prefix "1". Does not check whether the files is relevant for the server, 
/// but only if it was received (TCP without error). The maximum size of a backup file shall 
/// remain below BUFFER_SIZE (4kB) to be sent in 1 command only.
/// </summary>
/// <param name="psock">pointer to the valid socket (created and connected)</param>
/// <returns>-1 if the sending triggered an error, 0 if a file was sent successfully, 1 if no 
/// more  0[callsign].data files are to be sent.</returns>
#include <filesystem>
#include <regex>
namespace fs = std::filesystem;
int sendOldestBackup(SOCKET* psock)
{
    std::string fileToSend = "";
    std::string pathNfilename;
    std::string logmsg;
    std::ifstream bkfile;
    char buffer[BUFFER_SIZE] = { 0 };
    char line[BUFFER_SIZE];
    std::streamoff mjdOff = MJD_OFFSET;
    std::string strPattern(CALLSIGNPATTERN);
    std::regex regexPattern("0"+strPattern+"\\.data"); // filenames 0[callsign].data can be sent to server
                                             // filenames 1[callsign].data are those already sent to server
    double mjd = 0, earliest = -1;

    // selects file to be sent: the one with the oldest status. Same like in STC_restoreFromFolder()
    // BKP_PATH = "..\\Data\\"; // for debug only, due to working directory in debug mode
    for (const auto& fname : fs::directory_iterator(BKP_PATH)) {
        if (fname.is_regular_file()) {
            std::string filename = fname.path().filename().string();
            if (std::regex_match(filename, regexPattern)) {
                bkfile.open((BKP_PATH + filename), std::ios::in | std::ios::binary);
                if (!bkfile.is_open()) {
                    logThis(" -> File " + filename + ": error at opening!");
                    return -1;
                }
                else {
                    bkfile.seekg(mjdOff, std::ios::beg);
                    bkfile.read((char*)&mjd, sizeof(double));
                    if (earliest < 0 || mjd < earliest) {
                        earliest = mjd;
                        fileToSend = filename;
                    }
                }
                bkfile.close();
            }
        }
    }

    if (earliest < 0) {
        return +1; // no big deal, but <stop> will not be udpated
    }

    // Opens file. Reads file's size, sends this size, then sends its content to the server
    pathNfilename = BKP_PATH + fileToSend;
    logmsg = "File " + fileToSend;
    bkfile.open(pathNfilename, std::ios::in | std::ios::binary);
    if (!bkfile.is_open()) {
        logThis(" -> " + logmsg + ": error at opening!");
        return -1;
    }
    bkfile.seekg(0, std::ios::end); // positionned at the end
    int fileSize = (int)bkfile.tellg();
    strcpy(buffer, "*");
    memcpy(&buffer[1], &fileSize, sizeof(int));
    strcpy(&buffer[FILESIZE-1], "*");
    if (send(*psock, buffer, (int)FILESIZE, 0) == SOCKET_ERROR) {
        logThis(" -> " + logmsg + "'s size: send(..) failed, error = " + err_WSAGetLastError());
        bkfile.close();
        return -1;
    }
    bkfile.seekg(0, std::ios::beg); // positionned at the beginning
    bkfile.read(buffer, BUFFER_SIZE - FILESIZE);
    std::streamsize bytesRead = bkfile.gcount();
    if (send(*psock, buffer, bytesRead, 0) == SOCKET_ERROR) {
        logThis(" -> " + logmsg + "'s content: send(..) failed, error = " + err_WSAGetLastError());
        bkfile.close();
        return -1;
    }
    bkfile.close();

    // renaming
    int pathlength = BKP_PATH.length() + fileToSend.length() + 1; // +1 for the null terminator
    char oldFname[256];
    char newFname[256];
    strcpy(oldFname, (BKP_PATH + fileToSend).c_str());
    strcpy(newFname, oldFname);
    if (fileToSend.length() > 0) newFname[BKP_PATH.length()] = '1'; // new prefix
    if ((remove(newFname)) != 0) {/*logThis(logmsg + ": cleaning error " + std::to_string(errno));*/ }
    if ((rename(oldFname, newFname)) != 0) logThis(" -> " + logmsg + ": renaming error " + std::to_string(errno));
    else logThis("Socket " + std::to_string((int)*psock) + ": successfully sent " + fileToSend, true);
    return 0;
}