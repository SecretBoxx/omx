using Orbiter.Multiplayer;
using System.ServiceProcess;

namespace Orbiter
{
    public partial class ServerService : ServiceBase
    {
        private Server server;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Run(new ServiceBase[] { new ServerService() });
        }

        public ServerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            server = new Server(args);
            server.Start();
        }

        protected override void OnStop()
        {
            server.Stop();
        }
    }
}
