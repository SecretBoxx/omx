using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Orbiter
{
    [RunInstaller(true)]
    public class ServerServiceInstaller : Installer
    {
        private ServiceInstaller serviceInstaller;

        private ServiceProcessInstaller processInstaller;

        public ServerServiceInstaller()
        {
            processInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();

            processInstaller.Account = ServiceAccount.NetworkService;
            serviceInstaller.StartType = ServiceStartMode.Manual;
            serviceInstaller.ServiceName = "OMP server service";
            serviceInstaller.DisplayName = "OMPServer";
            serviceInstaller.Description = "Orbiter multiplayer project server application";

            Installers.Add(serviceInstaller);
            Installers.Add(processInstaller);
        }
    }
}