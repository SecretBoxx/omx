namespace Orbiter.Multiplayer
{
    /// <summary>
    /// Server information class.
    /// </summary>
    public class NTPServerInfo
    {
        private string address;

        /// <summary>
        /// Initializes a new instance of the <see cref="NTPServerInfo"/> class.
        /// </summary>
        /// <param name="address">The ip.</param>
        public NTPServerInfo(string address)
        {
            this.address = address;
        }

        /// <summary>
        /// Gets or sets the misses maximum.
        /// </summary>
        /// <value>The misses maximum.</value>
        public int MissesMaximum { get; set; }

        /// <summary>
        /// Gets or sets the delay.
        /// </summary>
        /// <value>The delay.</value>
        public int Delay { get; set; }

        /// <summary>
        /// Gets or sets the misses alarm.
        /// </summary>
        /// <value>The misses alarm.</value>
        public int MissesAlarm { get; set; }

        /// <summary>
        /// Gets the ip.
        /// </summary>
        /// <value>The ip.</value>
        public string Address
        {
            get { return address; }
        }

        /// <summary>
        /// Gets or sets the misses.
        /// </summary>
        /// <value>The misses.</value>
        public int Misses { get; set; }

        /// <summary>
        /// Gets or sets the suspensions.
        /// </summary>
        /// <value>The suspensions.</value>
        public int Suspensions { get; set; }
    }
}
