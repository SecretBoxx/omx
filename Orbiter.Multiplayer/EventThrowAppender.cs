using System;

namespace log4net.Appender
{
    /// <summary>
    /// Argument class for <seealso cref="EventThrowAppender"/> event.
    /// </summary>
    public class EventThrowAppenderEventArgs : EventArgs
    {
        private string message;

        /// <summary>
        /// Creates a new <seealso cref="EventThrowAppender"/> event argument
        /// with the specified message string.
        /// </summary>
        /// <param name="message">The message string.</param>
        public EventThrowAppenderEventArgs(string message)
        {
            this.message = message;
        }

        /// <summary>
        /// Returns the message string.
        /// </summary>
        public string Message
        {
            get { return message; }
        }
    }

    /// <summary>
    /// Delegate for the <seealso cref="EventThrowAppender"/> event.
    /// </summary>
    /// <param name="sender">The sender of the event. Normally the <seealso cref="EventThrowAppender"/> instance.</param>
    /// <param name="args">The <seealso cref="EventThrowAppenderEventArgs"/>.</param>
    public delegate void EventThrowAppenderDelegate(object sender, EventThrowAppenderEventArgs args);

    /// <summary>
    /// This appender simply forwards all logging events by means of a C#-event.
    /// </summary>
    public class EventThrowAppender : AppenderSkeleton
    {
        /// <summary>
        /// This event will be thrown whenever the logger receives a log-event.
        /// </summary>
        public event EventThrowAppenderDelegate Log;

        /// <summary>
        /// Appends the specified logging event.
        /// </summary>
        /// <param name="loggingEvent">The logging event.</param>
        protected override void Append(Core.LoggingEvent loggingEvent)
        {
            if (Log != null)
                Log(this, new EventThrowAppenderEventArgs(RenderLoggingEvent(loggingEvent)));
        }
    }
}
