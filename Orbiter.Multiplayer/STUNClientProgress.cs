using System;
using System.Net;

namespace Orbiter.Multiplayer
{
    public class STUNResponse : EventArgs
    {
        public STUNResponse()
        {
            NetworkType = STUNNetworkType.Open;
        }
        public STUNNetworkType NetworkType { get; set; }
        public IPEndPoint PublicEndPoint { get; set; }
        public string ProgressMessage { get; set; }
    }

    /// <summary>
    /// STUN ERROR-CODE defined in RFC 3489
    /// </summary>
    public class STUNErrorCode
    {
        public STUNErrorCode()
        {
            ReasonText = "";
            Code = 0;
        }
        public int Code { get; set; }
        public string ReasonText { get; set; }
    }

    public enum STUNNetworkType
    {
        Blocked,
        Open,
        Firewall,
        Cone,
        Restricted,
        PortRestricted,
        Symmetric,
        Unknown
    }

    /// <summary>
    /// Delegate for the progress event.
    /// </summary>
    public delegate void STUNClientProgressDelegate(object sender, STUNResponse args);
}
