using System;

namespace Orbiter.Multiplayer
{
    /// <summary>
    /// Kind of progress event.
    /// </summary>
    public enum NTPClientProgressKind
    {
        /// <summary>
        /// NTP client is waiting for next SNTP sample.
        /// </summary>
        Idle,
        /// <summary>
        /// SNTP sample is in progress.
        /// </summary>
        Pinging,
        /// <summary>
        /// SNTP server is blocked.
        /// </summary>
        Blocked,
        /// <summary>
        /// SNTP sample failed.
        /// </summary>
        Failed,
        /// <summary>
        /// SNTP sample retrieved successfully.
        /// </summary>
        Pinged,
        /// <summary>
        /// NTP client is offline, because no more unblocked servers are available.
        /// </summary>
        Offline,
        /// <summary>
        /// Synchronization cycle completed, skew is up-to-date.
        /// </summary>
        Synced,
    }

    /// <summary>
    /// Progress event arguments class
    /// </summary>
    public class NTPClientProgressEventArgs : EventArgs
    {
        private NTPClientProgressKind kind;

        /// <summary>
        /// Initializes a new instance of the <see cref="NTPClientProgressEventArgs"/> class.
        /// </summary>
        /// <param name="kind">The progress kind.</param>
        public NTPClientProgressEventArgs(NTPClientProgressKind kind)
        {
            this.kind = kind;
        }

        /// <summary>
        /// Gets the progress kind.
        /// </summary>
        /// <value>The progress kind.</value>
        public NTPClientProgressKind Kind
        {
            get { return kind; }
        }
    }

    /// <summary>
    /// Delegate for the progress event.
    /// </summary>
    public delegate void NTPClientProgressDelegate(object sender, NTPClientProgressEventArgs args);
}
