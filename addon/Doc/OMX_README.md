## **OMX**
OMX v1.3.0, GPLv2, issued 2024-11-02
(with OMPClient 2.10.x)

OMX is a fork and a continuation of the great OMP by Friedrich Kastner-Masilko (in GPLv2), a multi-user engine to be used with Orbiter 2016 created by Dr. Martin Scheiger (in MIT license).

OMX is the engine that propulses our own gameplay "Nex'Orbiter", a multi-user persistent universe to explore "at 1G" the solar system together, in the respect of the laws of physics and physiology. While OMX is in GPLv2, Nex'Orbiter is NOT open-source. Info & contact at https://nexorbiter.com/

OMP and its fork OMX are open-source under GNU GPL Version 2, and so must remain all their modifications:
- The GPL v2 is provided here as a text file.
- The source code of OMP, as forked, is available as the initial commit of the project https://gitlab.com/SecretBoxx/omx 
- The source code of the modified versions (labelled OMX) are available at the same URL, usually tagged when a new contribution or release is added.

2022-2024, Ass.Prof. Boris Segret, a.k.a. Boxx @ orbiter-forum.com

Powered by OMX v.1.3:

+================ Nex'Orbiter ==================+

|==== Dura Lex Gravitate, Sed Lex Gravitate ====|

+============= the Global Brain ================+

## New features:

See Doc/OMX_Features.md for the full log book.

# Backup and Restore

Backups to the server are run in background periodically (<1/second). For you, it is fully transparent: your Client uses server's port TCP+1, that must be open. For the moment, the restore function is operational but the procedure is not user-friendly. The documentation will be updated after the reconnection of a user with their backup vessel is functional.


# Offer-or-Take vessel in STC MFD

The OFR-TAK process (offer-or-take) is a key feature in OMX since v.1.1.0. It is a good way to gently close a session. Quick guide:
- from your vessel, select STC MFD > STC button > enter the remote Client name
- use "OFR" button to give your vessel, you can then disconnect.
- reconnect with *another* username,
- go to your former vessel: if not in the "ship" list, read OMX manual in Doc\ (\ls a u d, \trk <id>, %jump to <id>)
- from your former vessel, select MFD STC (your vessel is shown "NOT incontrol")
- select STC Button > "TAK" button, to take it back.
- see also OMX' manual in Doc\.


## Installation

Note: This OMX_README is about the OMX Engine only. For simplicity and demo, OMX is included in a package within Nex'Orbiter install. Nevertheless, the "addon" subfolder at Gitlab OMX's root provides the set of OMX required files only, already compiled, to customize your own installation of OMX. For developers, "addon" subfolder should be re-generated with the RELEASE configuration from VS2019 with "Generate Solution", after adapting your local paths in OMX_Dirs.props at root.

Step-by-step:
1. Install, if not already, the MS Visual C++ Redistributable vc_redist(*)
2. Re-install a vanilla version of Orbiter 2016 as a new folder (you can have multiple installs).
3. Download "Nex'Orbiter-w.OMX-x.x.x" zip, then go to zip's properties: if an "unlock" box is presented, unlock before unzipping.
4. Open the zip, then "yourOrbiterFolder". Copy the content and paste into Orbiter's root, keeping the folder structure. Accept all replacement.
5. Run Orbiter.exe, then in the launchpad:
	- for the first time, check your Video settings (you can unselect OMPClient in the Modules, then run the demo, exit completely and re-open Orbiter.exe)
	- then, select "OMPClient" in the Modules
6. In OMX Client window that opens, Network Tab:
	- setup your server as orbiterx.obspm.fr with port 1515
	- select a name (vessel name at the moment) and keep "toto" as a password (experimental)
7. In the launchpad, click on "Connect OMX" and follow the process.
8. If the display does not start, check troubleshooting tips, below.

(*) https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170

WARNING:
- some textures replace previous ones (that are renamed).
- for Devs: a modified "VesselAPI.h" is provided in 3rdParty\ and must be installed in ..\..\include of the Orbitersdk.

Report any bug or issue to the addon's thread (by Boxx) on Orbiter-forum.com, and consider filling in an issue in Gitlab (for developers). Provide a procedure that reproduces the bug. Just make sure you deselected any non requested addons, before reporting.

## More Support & Troubleshooting

- Orbiter-Forum: Boxx' threads and addons.
- Discord > Orbiter-forum server > OMP subgroup for live support (if somebody's there)
- Twitch for live streaming https://www.twitch.tv/orbinautboxx
- Nex'Orbiter website to discover what YOU could do https://nexorbiter.com/

Known issues:
- After a "vanilla" install, the first connections to OMX may block or crash: just exit completely and re-try.
- If you get a 'join error', follow the given indications: '\lv <name> <password>'.
- CTD when OMPClient is selected in Launchpad/Modules: you likely forgot to 'unlock' the zip file after downloading locally (see above, step 3).
- Connect OMX fails: check the title of the connection window, versions BEFORE 2.10 are NOT compatible. Use the latest version only.
- Blanks in the username should be avoided, e.g. "Bold Knight", as it may break the backup. Use only 1-word username for the moment. The aim is to connect with a call-sign in a near future.

## Roadmap

- The next priority is the server to restore vessels from their backup files automatically and to secure scalability.
- Doc/OMX_Features.md lists the additions since OMX v.1.0.0 and the known limitations.
- OMX_CICD.txt at project's root summarizes the expected tests before submitting new features.
- Below in "Test Report" section, the test results are commented as needed.
- OMX_ToDoList.txt at project's root presents a series of features highly desirable. Get inspired!

## Contributions

OMX is a fork of Face's OMP project 0.8.2, open source:
- here https://hg.osdn.net/view/orbitersoftware/OMP/archive/0.8.2.zip 
- or at the root (2022-09-17) of OMX' repository here https://gitlab.com/SecretBoxx/omx 

Parking Brake, by asbjos @ orbiter-forum.com is here:
- here https://www.orbiter-forum.com/resources/parking-brake.3105/ 
- or at the tag ParkingBrake (2023-12-17) of OMX' repository (commit b94c6d531695760de79d1cb89f4575282225188f)

ACKNOWLEGEMENT: OMX makes use of the following contributions by the community. Let them be thanked:
- ORBITER 2016, by Dr. Martin Schweiger, open source under MIT license
- OMP 0.8.2, by Friedrich Kastner-Masilko, open source under GPL v2 license, as modified into OMX
- ORL 1.1, by Mohd "computerex" Ali, Artyom "Artlav" Litvinovich, Tom "wehaveaproblem" Fisher, Ben "TSPenguin" Stickan, "RacerX", freeware (tbc)
- Parking Brake 03/2021, by asbjos @ orbiter-forum.com, GPLv2 granted to OMX on 2023-12-17

## Test Report for OMX v1.3

OMPClient v2.10 within OMX v.1.3.0 was tested wrt. OMX_CICD.txt (at project's root). Discrepancies:
- Documentation is accurate (although maybe not complete)? *NO!*: see above, feature "backup and restore".
- If too many Clients or(?) two many local vessels in a Client, new comers crash. This is still to be investigated.
