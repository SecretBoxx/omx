## **OMX**
Powered by OMX v1.2.5, GPLv2, issued 2024-09-17:

+================ Nex'Orbiter ==================+

|==== Dura Lex Gravitate, Sed Lex Gravitate ====|

+============= the Global Brain ================+

Contact Nex'Orbiter for any suggestions: https://nexorbiter.com or Boxx @ Orbiter-forum.

Report any bugs on OMX Addon's page: https://www.orbiter-forum.com/threads/nexorbiter-with-omx.41386/ 

## New features

OMX v1.3:
- 

OMX v1.2.5:
- implemented a periodical backup, disconnection-tolerant, of user's vessels sent to OMX server
- (restoring vessel's state from the backup will be done soon, see OMX_README)
- simplified time management (to be cont'd)
- fixed (?) the runtime issue for users without MS VS2019 installed
- fixed minor issues (log journals, planet collision, wording)

## Log book

OMX v1.1.1:
- fixed a security issue to keep silent when bots attemp to connect
- simplified VS2019 .vcxproj and .vsproj, with OMX_Dirs.props for local Paths

OMX v1.1.0:
- MFD STC shows if a vessel is locally controlled and if it is grounded (landed)
- Shorter GINFO sent (extended only in "transfer")
- Landed status reviewed and extended (inspired by ParkingBrake)
- More stability in vessel transfers, keeping thrust or parking status
- (fix) vessel status on non-spherical surfaces managed locally and shared remotely
- (fix) remote vessel suspended and/or jittering above surface
- (fix) while transfering, prevented the ghost from tumbling like crazy
- (for devs) Review of the Critical Sections calls, with Logging at level 5
- Note: user account management is implemented but not deployed yet.
Special Thanks for this version: jacquesmomo, asbjos

OMX v1.0.0:
- New Space Traffic Control MFD added (with poor GUI, ok)
- Offer/Take vessel was made functional, for alpha debug
- scenarii at Rochambeau airport and in deep Space (Mars, Phobos)
Special Thanks for this version: Face
