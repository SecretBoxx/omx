using System.Collections.Generic;

namespace Orbiter.Multiplayer.Client
{
    public class History<T>
    {
        private List<T> content;
        private int current;
        private T baseItem;

        public History(int capacity, T baseItem)
        {
            content = new List<T>(capacity);
            this.baseItem = baseItem;
            current = -1;
        }

        public void Add(T item)
        {
            if (content.Count == content.Capacity) content.RemoveAt(content.Count - 1);
            content.Insert(0, item);
            current = -1;
        }

        public T Up()
        {
            current++;
            if (current >= content.Count) current = content.Count - 1;
            if (current < 0) return baseItem;
            return content[current];
        }

        public T Down()
        {
            current--;
            if (current < 0)
            {
                current = -1;
                return baseItem;
            }
            return content[current];
        }
    }
}
