namespace Orbiter.Multiplayer.Client
{
    partial class StatusControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lLatencyTitle = new System.Windows.Forms.Label();
            this.lMJDTitle = new System.Windows.Forms.Label();
            this.lSkewTitle = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lMJDMax = new System.Windows.Forms.Label();
            this.lSkewMax = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lMJD = new System.Windows.Forms.Label();
            this.lSkew = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lMJDMin = new System.Windows.Forms.Label();
            this.lSkewMin = new System.Windows.Forms.Label();
            this.pBLaunchpad = new System.Windows.Forms.PictureBox();
            this.pBSimulation = new System.Windows.Forms.PictureBox();
            this.pBSimulationConnected = new System.Windows.Forms.PictureBox();
            this.pBLaunchpadConnected = new System.Windows.Forms.PictureBox();
            this.pBLaunchpadClock = new System.Windows.Forms.PictureBox();
            this.pBLaunchpadFadingClock = new System.Windows.Forms.PictureBox();
            this.pBLaunchpadPing = new System.Windows.Forms.PictureBox();
            this.pBSimulationPing1 = new System.Windows.Forms.PictureBox();
            this.pBSimulationPing2 = new System.Windows.Forms.PictureBox();
            this.pBSimulationPing3 = new System.Windows.Forms.PictureBox();
            this.pBSimulationClock = new System.Windows.Forms.PictureBox();
            this.pBSimulationFadingClock = new System.Windows.Forms.PictureBox();
            this.pBSimulationPing = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationConnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpadConnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpadClock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpadFadingClock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpadPing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationPing1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationPing2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationPing3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationClock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationFadingClock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationPing)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.lLatencyTitle, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lMJDTitle, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lSkewTitle, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lMJDMax, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lSkewMax, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lMJD, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lSkew, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lMJDMin, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lSkewMin, 2, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(344, 152);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lLatencyTitle
            // 
            this.lLatencyTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lLatencyTitle.Location = new System.Drawing.Point(3, 0);
            this.lLatencyTitle.Name = "lLatencyTitle";
            this.lLatencyTitle.Size = new System.Drawing.Size(108, 38);
            this.lLatencyTitle.TabIndex = 0;
            this.lLatencyTitle.Text = "Latency[ms]";
            this.lLatencyTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lMJDTitle
            // 
            this.lMJDTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lMJDTitle.Location = new System.Drawing.Point(117, 0);
            this.lMJDTitle.Name = "lMJDTitle";
            this.lMJDTitle.Size = new System.Drawing.Size(108, 38);
            this.lMJDTitle.TabIndex = 0;
            this.lMJDTitle.Text = "MJD[s]";
            this.lMJDTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lSkewTitle
            // 
            this.lSkewTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lSkewTitle.Location = new System.Drawing.Point(231, 0);
            this.lSkewTitle.Name = "lSkewTitle";
            this.lSkewTitle.Size = new System.Drawing.Size(110, 38);
            this.lSkewTitle.TabIndex = 0;
            this.lSkewTitle.Text = "Skew[�s/s]";
            this.lSkewTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 38);
            this.label4.TabIndex = 0;
            this.label4.Text = "Sample";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lMJDMax
            // 
            this.lMJDMax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lMJDMax.Location = new System.Drawing.Point(117, 38);
            this.lMJDMax.Name = "lMJDMax";
            this.lMJDMax.Size = new System.Drawing.Size(108, 38);
            this.lMJDMax.TabIndex = 0;
            this.lMJDMax.Text = "Sample";
            this.lMJDMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lSkewMax
            // 
            this.lSkewMax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lSkewMax.Location = new System.Drawing.Point(231, 38);
            this.lSkewMax.Name = "lSkewMax";
            this.lSkewMax.Size = new System.Drawing.Size(110, 38);
            this.lSkewMax.TabIndex = 0;
            this.lSkewMax.Text = "Sample";
            this.lSkewMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 38);
            this.label7.TabIndex = 0;
            this.label7.Text = "Sample";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lMJD
            // 
            this.lMJD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lMJD.Location = new System.Drawing.Point(117, 76);
            this.lMJD.Name = "lMJD";
            this.lMJD.Size = new System.Drawing.Size(108, 38);
            this.lMJD.TabIndex = 0;
            this.lMJD.Text = "Sample";
            this.lMJD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lSkew
            // 
            this.lSkew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lSkew.Location = new System.Drawing.Point(231, 76);
            this.lSkew.Name = "lSkew";
            this.lSkew.Size = new System.Drawing.Size(110, 38);
            this.lSkew.TabIndex = 0;
            this.lSkew.Text = "Sample";
            this.lSkew.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 114);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 38);
            this.label10.TabIndex = 0;
            this.label10.Text = "Sample";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lMJDMin
            // 
            this.lMJDMin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lMJDMin.Location = new System.Drawing.Point(117, 114);
            this.lMJDMin.Name = "lMJDMin";
            this.lMJDMin.Size = new System.Drawing.Size(108, 38);
            this.lMJDMin.TabIndex = 0;
            this.lMJDMin.Text = "Sample";
            this.lMJDMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lSkewMin
            // 
            this.lSkewMin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lSkewMin.Location = new System.Drawing.Point(231, 114);
            this.lSkewMin.Name = "lSkewMin";
            this.lSkewMin.Size = new System.Drawing.Size(110, 38);
            this.lSkewMin.TabIndex = 0;
            this.lSkewMin.Text = "Sample";
            this.lSkewMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pBLaunchpad
            // 
            this.pBLaunchpad.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBLaunchpad.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_l;
            this.pBLaunchpad.Location = new System.Drawing.Point(0, 161);
            this.pBLaunchpad.Name = "pBLaunchpad";
            this.pBLaunchpad.Size = new System.Drawing.Size(350, 100);
            this.pBLaunchpad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBLaunchpad.TabIndex = 1;
            this.pBLaunchpad.TabStop = false;
            // 
            // pBSimulation
            // 
            this.pBSimulation.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBSimulation.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_s;
            this.pBSimulation.Location = new System.Drawing.Point(0, 161);
            this.pBSimulation.Name = "pBSimulation";
            this.pBSimulation.Size = new System.Drawing.Size(350, 100);
            this.pBSimulation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBSimulation.TabIndex = 2;
            this.pBSimulation.TabStop = false;
            // 
            // pBSimulationConnected
            // 
            this.pBSimulationConnected.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBSimulationConnected.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_ss1;
            this.pBSimulationConnected.Location = new System.Drawing.Point(0, 161);
            this.pBSimulationConnected.Name = "pBSimulationConnected";
            this.pBSimulationConnected.Size = new System.Drawing.Size(80, 100);
            this.pBSimulationConnected.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBSimulationConnected.TabIndex = 3;
            this.pBSimulationConnected.TabStop = false;
            // 
            // pBLaunchpadConnected
            // 
            this.pBLaunchpadConnected.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBLaunchpadConnected.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_ls1;
            this.pBLaunchpadConnected.Location = new System.Drawing.Point(0, 161);
            this.pBLaunchpadConnected.Name = "pBLaunchpadConnected";
            this.pBLaunchpadConnected.Size = new System.Drawing.Size(80, 100);
            this.pBLaunchpadConnected.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBLaunchpadConnected.TabIndex = 4;
            this.pBLaunchpadConnected.TabStop = false;
            // 
            // pBLaunchpadClock
            // 
            this.pBLaunchpadClock.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBLaunchpadClock.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_lc;
            this.pBLaunchpadClock.Location = new System.Drawing.Point(280, 185);
            this.pBLaunchpadClock.Name = "pBLaunchpadClock";
            this.pBLaunchpadClock.Size = new System.Drawing.Size(52, 52);
            this.pBLaunchpadClock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBLaunchpadClock.TabIndex = 5;
            this.pBLaunchpadClock.TabStop = false;
            // 
            // pBLaunchpadFadingClock
            // 
            this.pBLaunchpadFadingClock.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBLaunchpadFadingClock.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_lfc;
            this.pBLaunchpadFadingClock.Location = new System.Drawing.Point(280, 185);
            this.pBLaunchpadFadingClock.Name = "pBLaunchpadFadingClock";
            this.pBLaunchpadFadingClock.Size = new System.Drawing.Size(52, 52);
            this.pBLaunchpadFadingClock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBLaunchpadFadingClock.TabIndex = 6;
            this.pBLaunchpadFadingClock.TabStop = false;
            // 
            // pBLaunchpadPing
            // 
            this.pBLaunchpadPing.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBLaunchpadPing.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_lp;
            this.pBLaunchpadPing.Location = new System.Drawing.Point(250, 177);
            this.pBLaunchpadPing.Name = "pBLaunchpadPing";
            this.pBLaunchpadPing.Size = new System.Drawing.Size(27, 43);
            this.pBLaunchpadPing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBLaunchpadPing.TabIndex = 7;
            this.pBLaunchpadPing.TabStop = false;
            // 
            // pBSimulationPing1
            // 
            this.pBSimulationPing1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBSimulationPing1.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_p1;
            this.pBSimulationPing1.Location = new System.Drawing.Point(50, 161);
            this.pBSimulationPing1.Name = "pBSimulationPing1";
            this.pBSimulationPing1.Size = new System.Drawing.Size(22, 37);
            this.pBSimulationPing1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBSimulationPing1.TabIndex = 8;
            this.pBSimulationPing1.TabStop = false;
            // 
            // pBSimulationPing2
            // 
            this.pBSimulationPing2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBSimulationPing2.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_p2;
            this.pBSimulationPing2.Location = new System.Drawing.Point(90, 161);
            this.pBSimulationPing2.Name = "pBSimulationPing2";
            this.pBSimulationPing2.Size = new System.Drawing.Size(30, 45);
            this.pBSimulationPing2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBSimulationPing2.TabIndex = 9;
            this.pBSimulationPing2.TabStop = false;
            // 
            // pBSimulationPing3
            // 
            this.pBSimulationPing3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBSimulationPing3.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_p3;
            this.pBSimulationPing3.Location = new System.Drawing.Point(140, 161);
            this.pBSimulationPing3.Name = "pBSimulationPing3";
            this.pBSimulationPing3.Size = new System.Drawing.Size(36, 51);
            this.pBSimulationPing3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBSimulationPing3.TabIndex = 10;
            this.pBSimulationPing3.TabStop = false;
            // 
            // pBSimulationClock
            // 
            this.pBSimulationClock.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBSimulationClock.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_sc;
            this.pBSimulationClock.Location = new System.Drawing.Point(280, 185);
            this.pBSimulationClock.Name = "pBSimulationClock";
            this.pBSimulationClock.Size = new System.Drawing.Size(52, 52);
            this.pBSimulationClock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBSimulationClock.TabIndex = 11;
            this.pBSimulationClock.TabStop = false;
            // 
            // pBSimulationFadingClock
            // 
            this.pBSimulationFadingClock.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBSimulationFadingClock.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_sfc;
            this.pBSimulationFadingClock.Location = new System.Drawing.Point(280, 185);
            this.pBSimulationFadingClock.Name = "pBSimulationFadingClock";
            this.pBSimulationFadingClock.Size = new System.Drawing.Size(52, 52);
            this.pBSimulationFadingClock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBSimulationFadingClock.TabIndex = 12;
            this.pBSimulationFadingClock.TabStop = false;
            // 
            // pBSimulationPing
            // 
            this.pBSimulationPing.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pBSimulationPing.Image = global::Orbiter.Multiplayer.Client.EmbeddedRessources.tile_sp;
            this.pBSimulationPing.Location = new System.Drawing.Point(250, 177);
            this.pBSimulationPing.Name = "pBSimulationPing";
            this.pBSimulationPing.Size = new System.Drawing.Size(27, 43);
            this.pBSimulationPing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBSimulationPing.TabIndex = 13;
            this.pBSimulationPing.TabStop = false;
            // 
            // StatusControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pBLaunchpadPing);
            this.Controls.Add(this.pBLaunchpadClock);
            this.Controls.Add(this.pBLaunchpadFadingClock);
            this.Controls.Add(this.pBSimulationPing);
            this.Controls.Add(this.pBSimulationFadingClock);
            this.Controls.Add(this.pBSimulationClock);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.pBSimulationPing2);
            this.Controls.Add(this.pBSimulationPing3);
            this.Controls.Add(this.pBLaunchpadConnected);
            this.Controls.Add(this.pBSimulationPing1);
            this.Controls.Add(this.pBSimulationConnected);
            this.Controls.Add(this.pBLaunchpad);
            this.Controls.Add(this.pBSimulation);
            this.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StatusControl";
            this.Size = new System.Drawing.Size(350, 261);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationConnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpadConnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpadClock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpadFadingClock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLaunchpadPing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationPing1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationPing2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationPing3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationClock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationFadingClock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBSimulationPing)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lLatencyTitle;
        private System.Windows.Forms.Label lMJDTitle;
        private System.Windows.Forms.Label lSkewTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lMJDMax;
        private System.Windows.Forms.Label lSkewMax;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lMJD;
        private System.Windows.Forms.Label lSkew;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lMJDMin;
        private System.Windows.Forms.Label lSkewMin;
        private System.Windows.Forms.PictureBox pBLaunchpad;
        private System.Windows.Forms.PictureBox pBSimulation;
        private System.Windows.Forms.PictureBox pBSimulationConnected;
        private System.Windows.Forms.PictureBox pBLaunchpadConnected;
        private System.Windows.Forms.PictureBox pBLaunchpadClock;
        private System.Windows.Forms.PictureBox pBLaunchpadFadingClock;
        private System.Windows.Forms.PictureBox pBLaunchpadPing;
        private System.Windows.Forms.PictureBox pBSimulationPing1;
        private System.Windows.Forms.PictureBox pBSimulationPing2;
        private System.Windows.Forms.PictureBox pBSimulationPing3;
        private System.Windows.Forms.PictureBox pBSimulationClock;
        private System.Windows.Forms.PictureBox pBSimulationFadingClock;
        private System.Windows.Forms.PictureBox pBSimulationPing;
    }
}
