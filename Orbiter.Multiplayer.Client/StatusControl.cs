using System.Windows.Forms;

namespace Orbiter.Multiplayer.Client
{
    public partial class StatusControl : UserControl
    {
        private MethodInvoker update;
        private Database database;

        public StatusControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the database.
        /// </summary>
        /// <param name="database">The database.</param>
        public void SetDatabase(Database database)
        {
            this.database = database;

            /*database.StatusDataChanged+=delegate
            {
                if (InvokeRequired) Invoke(update);
                else update.Invoke();
            }
            ;*/
        }

        public void Init(bool simulation)
        {
            if (simulation)
            {
                pBSimulation.Visible = true;
                pBSimulationClock.Visible = true;
                pBSimulationConnected.Visible = false;
                pBSimulationFadingClock.Visible = false;
                pBSimulationPing.Visible = false;
                pBSimulationPing1.Visible = false;
                pBSimulationPing2.Visible = false;
                pBSimulationPing3.Visible = false;

                pBLaunchpad.Visible = false;
                pBLaunchpadClock.Visible = false;
                pBLaunchpadConnected.Visible = false;
                pBLaunchpadFadingClock.Visible = false;
                pBLaunchpadPing.Visible = false;

                update = new MethodInvoker(
                    delegate
                        {
                            lMJDMax.Text = database.MjdMax.ToString();
                            lMJDMin.Text = database.MjdMin.ToString();
                            lMJD.Text = database.Mjd.ToString();
                            lSkewMax.Text = database.SkewMax.ToString();
                            lSkewMin.Text = database.SkewMin.ToString();
                            lSkew.Text = database.Skew.ToString();
                            pBSimulationPing.Visible = database.ClockPulseVisible;
                            pBSimulationConnected.Visible = database.SystemVisible;
                            switch (database.ClockVisibility)
                            {
                                case ClockVisibility.Hidden:
                                    pBSimulationFadingClock.Visible = false;
                                    pBSimulationClock.Visible = false;
                                    break;
                                case ClockVisibility.Fading:
                                    pBSimulationFadingClock.Visible = true;
                                    pBSimulationClock.Visible = false;
                                    break;
                                case ClockVisibility.Visible:
                                    pBSimulationFadingClock.Visible = false;
                                    pBSimulationClock.Visible = true;
                                    break;
                            }
                            switch (database.Pulse)
                            {
                                case 1:
                                    pBSimulationPing1.Visible = true;
                                    pBSimulationPing2.Visible = false;
                                    pBSimulationPing3.Visible = false;
                                    break;
                                case 2:
                                    pBSimulationPing1.Visible = false;
                                    pBSimulationPing2.Visible = true;
                                    pBSimulationPing3.Visible = false;
                                    break;
                                case 3:
                                    pBSimulationPing1.Visible = false;
                                    pBSimulationPing2.Visible = false;
                                    pBSimulationPing3.Visible = true;
                                    break;
                                default:
                                    pBSimulationPing1.Visible = false;
                                    pBSimulationPing2.Visible = false;
                                    pBSimulationPing3.Visible = false;
                                    break;
                            }
                        });
            }
            else
            {
                pBSimulation.Visible = false;
                pBSimulationClock.Visible = false;
                pBSimulationConnected.Visible = false;
                pBSimulationFadingClock.Visible = false;
                pBSimulationPing.Visible = false;
                pBSimulationPing1.Visible = false;
                pBSimulationPing2.Visible = false;
                pBSimulationPing3.Visible = false;

                pBLaunchpad.Visible = true;
                pBLaunchpadClock.Visible = true;
                pBLaunchpadConnected.Visible = false;
                pBLaunchpadFadingClock.Visible = false;
                pBLaunchpadPing.Visible = false;

                update = new MethodInvoker(
                    delegate
                        {
                            lMJDMax.Text = database.MjdMax.ToString();
                            lMJDMin.Text = database.MjdMin.ToString();
                            lMJD.Text = database.Mjd.ToString();
                            lSkewMax.Text = database.SkewMax.ToString();
                            lSkewMin.Text = database.SkewMin.ToString();
                            lSkew.Text = database.Skew.ToString();
                            pBLaunchpadPing.Visible = database.ClockPulseVisible;
                            pBLaunchpadConnected.Visible = database.SystemVisible;
                            switch (database.ClockVisibility)
                            {
                                case ClockVisibility.Hidden:
                                    pBLaunchpadFadingClock.Visible = false;
                                    pBLaunchpadClock.Visible = false;
                                    break;
                                case ClockVisibility.Fading:
                                    pBLaunchpadFadingClock.Visible = true;
                                    pBLaunchpadClock.Visible = false;
                                    break;
                                case ClockVisibility.Visible:
                                    pBLaunchpadFadingClock.Visible = false;
                                    pBLaunchpadClock.Visible = true;
                                    break;
                            }
                        });
            }
        }
    }
}
