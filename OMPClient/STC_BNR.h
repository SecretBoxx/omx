// ===================================================================
//              "Space Traffic Control" for OMX
//              refactoring parts of OMP Clients
//
//   (c) 11/2022-2024+ Boris Segret, GPLv2 license (due to OMP)
//
// This header file declares common resources for OMPClient.cpp,
// STC_*.cpp and OMX_backup*.cpp, and defines routines printTime(),
// err_WSAGetLastError(), logThis(..).
// ===================================================================

#ifndef STC_BNR_H
#define STC_BNR_H

#define STRICT
#include "orbitersdk.h"

std::string EXE_PATH = "Server\\Windows\\"; // as seen from OMPClient at runtime (OMX_backup....exe)
std::string BKP_PATH = "Server\\Data\\"; // as seen from OMX_backupClient, once launched by OMPClient
char    LOC_PATH[10] = "..\\Data\\"; // as seen from ServerConsole.exe and OMX_backup....exe
#define _NOW_ printTime()
std::string err_WSAGetLastError();
std::string printTime();
void logThis(bool toServer, const std::string& msg, bool checkforDuplicate = false);

union UDPpacket	// safe max size is 508 bytes
{	// see also https://learn.microsoft.com/en-us/cpp/cpp/fundamental-types-cpp?view=msvc-170 
	// DWORD = unsigned long integer = 4 bytes
	struct
	{
		char			flags;	// 1 byte
		char			size;	// 1 byte
	unsigned short int	reference;	 // 2 bytes
		unsigned int	ID;	 // 4 bytes
		double			MJD;	 // 8 bytes
		double			tsSent;	 // 8 bytes
		VECTOR3			pos;	 // 24 bytes
		VECTOR3			vel;
		VECTOR3			acc;
		VECTOR3			arot;
		VECTOR3			vrot;
	}		content;
	struct
	{
		char			flags;
		char			Global4Local;
	unsigned short int	reserve2;
	unsigned short int	GlobalID;
	unsigned short int	LocalID;
		union
		{
			//for server KA
			double		MJD;
			//for neighbour KA
			struct
			{
		signed char		main;
		signed char		hover;
		signed char		pitch;
		signed char		yaw;
		signed char		bank;
		signed char		sideward;
		signed char		upward;
		signed char		forward;
			};
		};
		double			tsSent;
	}		contentKA;
	struct
	{
		char			flags;
		char			reserve3;
	unsigned short int	id;
	unsigned short int	id2;
	unsigned short int	id3;
		double			tsSent;
	}		contentPG;
	struct
	{
		char			flags;
		char			reserve1;
	unsigned short int	Source;
	unsigned short int	reserve4;
	unsigned short int	port;
	unsigned long int	ip;
		double			MJD;
		double			tsSent;
	}		contentGI;
	char	buffer[	144];
};

#define MJD_OFFSET		2*sizeof(char) + sizeof(unsigned short int) + sizeof(unsigned int) // on Client's side
#define SRV_OFFSET		6 // Server's side added offset due to the addition of '*<size>*' in '9...' backupfiles
#define BUFFER_SIZE		(4*1024)  // max size of a backup file
#define FILESIZE		(2+sizeof(int))  // size of the field "*NN*" where NN = backup file's size
#define ownerNameP		0x0C0  // addr starting after extendedPacket (i.e. owner's Name)
#define ownerLengthP	0x0A9  // addr in extendedPacket for the length of owner's Name
#define vnameLengthP	0x0AA  // addr in extendedPacket for the length of vessel's Name
#define CALLSIGNPATTERN	"[A-Za-z][A-Za-z0-9]*-?[A-Za-z0-9]*" // filename = callsigns, WITHOUT prefix (1 char)
struct extendedPacket // 144+52=196 Bytes (+4 bytes?! => sizeof(...)=200i64)
{
	UDPpacket pac;	// 144 Bytes (0x000..08F)
	double surf_lng;	// 8 B (0x090..097)
	double surf_lat;	// 8 B (0x098..09F)
	double surf_hdg;	// 8 B (0x0A0..0A7)
	unsigned __int8 status;		// 1 B (0x0A8)
	unsigned __int8 ownerNameLength; // 1 B (0x0A9)
	unsigned __int8 vesselNameLength; // 1 B (0x0AA)
	unsigned __int8 vClassNameLength; // 1 B (0x0AB)
	DWORD nfuel;	// 4 B (0x0AC)
	DWORD nthruster;	// 4 B (0x0B0)
	DWORD ndockinfo;	// 4 B (0x0B4)
	UINT nanims;		// 4 B (0x0B8)
	DWORD xpdr;	// 4 B (0x0BC)
};

#include <iostream>
/// <summary>
/// Transforms a error code into its keyword for logging.
/// </summary>
std::string err_WSAGetLastError() {
	switch (int errNb = WSAGetLastError()) {
	case WSANOTINITIALISED: return "WSANOTINITIALISED";
	case WSAEFAULT: return "WSAEFAULT";
	case WSAENETDOWN: return "WSAENETDOWN";
	case WSAEINVAL: return "WSAEINVAL";
	case WSAEINTR: return "WSAEINTR";
	case WSAEINPROGRESS: return "WSAEINPROGRESS" ;
	case WSAENOTSOCK: return "WSAENOTSOCK";
	case WSAEACCES: return "WSAEACCES";
	case WSAENETRESET: return "WSAENETRESET";
	case WSAENOBUFS: return "WSAENOBUFS"; 
	case WSAENOTCONN: return "WSAENOTCONN";
	case WSAEOPNOTSUPP: return "WSAEOPNOTSUPP";
	case WSAESHUTDOWN: return "WSAESHUTDOWN";
	case WSAEWOULDBLOCK: return "WSAEWOULDBLOCK";
	case WSAEMSGSIZE: return "WSAEMSGSIZE";
	case WSAEHOSTUNREACH: return "WSAEHOSTUNREACH";
	case WSAECONNABORTED: return "WSAECONNABORTED";
	case WSAECONNRESET: return "WSAECONNRESET";
	case WSAETIMEDOUT: return "WSAETIMEDOUT";
	default:
		char errMsg[20];
		sprintf(errMsg, "Error WSA #%d(?)", errNb);
		return (std::string)errMsg;
	}
};


/// <summary>
/// Formats the current time into text for logging.
/// </summary>
/// <returns></returns>
std::string printTime() {
	time_t logt;
	char cTime[25];
	logt = time(NULL);
	std::strftime(cTime, 22, "%Y-%m-%d %H:%M:%S", localtime(&logt));
	std::string cTT(cTime); cTT = cTT + " ";
	return cTT;
}

#include <filesystem>
#include <vector>

/// <summary>
/// Loggs msg either to the server's daily log journal or to the client's log journal. If an earlier line
/// contains msg (provided checkforDuplicate), the previous line is removed and the new one is appended.
/// Names of log files are hard-coded.
/// </summary>
/// <param name="toServer">log file shall be server's one (true) or client's one (false)</param>
/// <param name="msg">log message to be added</param>
/// <param name="checkforDuplicate">remove same earlier msg and append new one</param>
void logThis(bool toServer, const std::string& msg, bool checkforDuplicate) {
	char logFile[100];
	char strnow[25];
	char today[11];
	char ch;
	std::fstream log;
	std::streampos log_end, prev_eol, pos, line_start, remaining_size;
	std::string line;
	std::streamsize line_length;
	std::vector<char> buffer;

	sprintf(strnow, "%s ", _NOW_.c_str());
	strncpy(today, strnow, 10); today[10] = '\0';
	if (toServer) sprintf(logFile, "backupLog(S)_%s.txt", today);
	else {
		strcpy(logFile, "Server\\Windows\\backupLog(C).txt");
		std::filesystem::file_status s = std::filesystem::status(logFile);
		if (std::filesystem::exists(s)) if (std::filesystem::file_size(logFile) > 1E5) {
			if ((remove("Server\\Windows\\backupLog(C)_prev.txt")) != 0) {/*logThis(logmsg + ": cleaning error " + std::to_string(errno));*/ }
			if ((rename(logFile, "Server\\Windows\\backupLog(C)_prev.txt")) != 0) { std::cerr << std::string(logFile) + ": renaming error " << errno << std::endl; }
			else logThis(false, std::string("backupLog(C) continuing, previous log in backupLog(C)_prev.txt"));
		}
	}

	if (checkforDuplicate) {
		log.open(logFile, std::ios::in | std::ios::binary);
		if (!log.is_open()) {
			log.close();
			std::cerr << "Cannot open logFile \"" << logFile << "\" to check for duplicate." << std::endl;
			return;
		}
		log.seekg(0, std::ios::end); log_end = log.tellg(); // -(std::streampos)1; // last readable position
		prev_eol = log_end;
		pos = log_end;
		while (pos > 0) {
			// reads the logFile backward until an EOL is found
			pos -= 1;  // Decrement position safely
			log.seekg(pos);
			log.get(ch);
			if ((ch == '\n' || pos == 0)) {// && (pos < log_end) - (std::streampos)1)) {
				// if EOL (or start of the file) AND this EOL is not the last char of the file
				// then checks for msg in the previous line
				line_start = (pos == 0) ? pos : (pos + (std::streampos)1); // start of the line where the &msg could match
				line_length = prev_eol - line_start; // length of that matching line
				std::string current_line(line_length, '\0');
				log.seekg(line_start);
				log.read(&current_line[0], line_length);
				// if msg is in the line, copies remaining content, after the current line, from the file into a buffer
				if ((current_line.find(msg)) != std::string::npos) {
					// indeed that line matches &msg: a buffer is filled in with what is *after* that line (from prev_eol)
					buffer.clear();
					log.seekg(prev_eol);  // Go to the 1st char of the next line (after EOL)
					remaining_size = log_end - prev_eol;
					if (remaining_size < 0) break;
					buffer.resize(remaining_size);
					log.read(buffer.data(), remaining_size);
					if (log.gcount() != remaining_size) {
						logThis(toServer, "error: buffer size is wrong to append to logFile!");
						break;
					}
					log.close();
					// Now the file is truncated, then the buffer is appended
					std::filesystem::resize_file(logFile, line_start - (std::streampos)1);
					log.open(logFile, std::ios::out | std::ios::app | std::ios::binary);
					log.write(buffer.data(), remaining_size);
					break;
				}
				// msg was not found, continues backward (before the current line)
				prev_eol = pos;
			}
		}
	}
	if (log.is_open()) log.close();

	// then, adds msg:
	log.open(logFile, std::ios::out | std::ios::app | std::ios::binary);
	if (!log.is_open()) {
		log.close();
		std::cerr << "Cannot open logFile \"" << logFile << "\" to append to it." << std::endl;
		return;
	}
	log.write(strnow, 20);
	log.write(msg.data(), msg.size());
	ch = '\n'; log.write(&ch, 1);
	log.close();
	return;
}

#endif // !STC_BNR_H