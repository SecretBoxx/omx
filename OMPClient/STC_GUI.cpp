// ==============================================================
//              "Space Traffic Control" MFD for OMX
// part of STC
//   (c) 11/2022-2024+ Boris Segret, due to OMP, GPL license applies
// incldued:
//  STC::STC(DWORD w, DWORD h, VESSEL *vessel)
//  STC::~STC()
//  char * STC::ButtonLabel (int bt)
//  int  STC::ButtonMenu (const MFDBUTTONMENU **menu) const
//  bool STC::ConsumeButton(int bt, int event)
//  bool STC::ConsumeKeyBuffered(DWORD key)
//  bool STC::DataInput(void* id, char* str, void* data)
//  bool STC::DataInput(void* id, char* str)
//  int  STC::MsgProc(UINT msg, UINT mfd, WPARAM wparam, LPARAM lparam)
//  bool STC::Update(oapi::Sketchpad* skp)
// ==============================================================

// ==============================================================
// API interface

/* // both InitModule and ExitModule were merged with OMPClient.cpp
DLLCLBK void InitModule (HINSTANCE hDLL)
DLLCLBK void ExitModule (HINSTANCE hDLL)
*/

// Constructor
STC::STC(DWORD w, DWORD h, VESSEL *vessel)
: MFD2 (w, h, vessel)
{
	font  = oapiCreateFont(h / mxMFDlines, true, "Sans", FONT_NORMAL, 0);
	fontB = oapiCreateFont(h / mxMFDlines, true, "Sans", FONT_BOLD, 0);
	// Add MFD initialisation here
}

// Destructor
STC::~STC()
{
	oapiReleaseFont(font);
	oapiReleaseFont(fontB);
	// Add MFD cleanup code here
}

// ============================================================================================================
// create and return button labels for the MFD

char * STC::ButtonLabel (int bt)
{
	// The labels for the buttons used by our MFD mode
	static char* label[12] =
	  { "STC", "LST", "---", "---", "OFR", "GIV", "ALL", "MAX", "---","---", "BCK", "TAK" };
	return (bt < ARRAYSIZE(label) ? label[bt] : 0);
}

// create and return button menus
int STC::ButtonMenu (const MFDBUTTONMENU **menu) const
{
	// The menu descriptions for the two buttons
	static const MFDBUTTONMENU mnu[12] = {
		{"Select STC", NULL, NULL},
		{"List of STCs", NULL, NULL},
		{"(provision)", NULL, NULL},
		{"(provision)", NULL, NULL},
		{"Offer this/ALL", "vessel(s) to STC", NULL},
		{"Give it/them", NULL, NULL},
		{"All/Single", "trigger", NULL},
		{"(provision)", NULL, NULL},
		{"(provision)", NULL, NULL},
		{"TESTING", "(admin only)", NULL},
		{"Own vessel", "to take back", NULL},
		{"Take it back", NULL, NULL}
	};
	if (menu) *menu = mnu;
	return 12; // return the number of buttons used
}

//
bool STC::ConsumeButton(int bt, int event)
{
	static const DWORD btkey[12] = { OAPI_KEY_S, OAPI_KEY_L, OAPI_KEY_SUBTRACT, OAPI_KEY_P, OAPI_KEY_O, OAPI_KEY_G,
		OAPI_KEY_A, OAPI_KEY_M, OAPI_KEY_ADD, OAPI_KEY_W, OAPI_KEY_B, OAPI_KEY_T };
	//	OAPI_KEY_A, OAPI_KEY_M, OAPI_KEY_ADD, NULL, OAPI_KEY_B, OAPI_KEY_T };
if (event & PANEL_MOUSE_LBDOWN) {
		return ConsumeKeyBuffered(btkey[bt]);
	}
	return false;
}

bool STC::ConsumeKeyBuffered(DWORD key)
{
	char str[256] = "";
	char logLine[256] = "";
	struct IDload* entry;
	VESSEL* focus = NULL;
	OBJHANDLE focobj = oapiGetFocusObject();
	if (oapiIsVessel(focobj)) focus = oapiGetVesselInterface(focobj);

	switch (key) {

	case OAPI_KEY_W:
		tlogging(1, "TESTING: running STC_restoreFromFolder");
		if (!STC_restoring) { // (to do) : prevent this from running if NOT Admin
			STC_progress = init;
			STC_restoring = true;
		}
		return true;

	case OAPI_KEY_S:	// Select STC to receive Hosting request
		tlogging(5, "STC: selecting a Space Traffic Controler");
		dialogSelection = SelectgSTC;
		oapiOpenInputBox("STC (e.g.ORBIX):", DataInput, 0, 32, (void*)this);
		// now STC::hostSTC =  handler to the sollicited STC (or NULL)
		return true;

	case OAPI_KEY_L:	// List STCs
		return true;

	case OAPI_KEY_P:	// (provision) not needed yet
		return true;

	case OAPI_KEY_A:	// All/This trigger
		STC_ALLtrigger = !STC_ALLtrigger;
		STC_GOallowed = false;
		return true;

	case OAPI_KEY_M:	// Max number of hosts
		return true;
	case OAPI_KEY_SUBTRACT:	// Move down
		return true;
	case OAPI_KEY_ADD:	// Move up
		return true;

	case OAPI_KEY_O:	// Offer user's vessel(s) to STC (here = offering client)
		if (STC_takingBack) return true;
		STC_GOallowed = !STC_GOallowed; // on/off, first reinits the variables
		STC_offergInProcess = false; // wil turn true with button "GO"
		STC_clearToOffer = true;
		STC_offergWatchdog = 0.;
		STC_globalId = 0;
		sprintf(STC_offrdName, "NN-iii");
		if (STC_GOallowed) if (!STC_ALLtrigger) {
			if (focus != NULL) {
				sprintf(str, "%p", focus->GetHandle());
				if ((entry = (struct IDload*)clientData.GlobalsByHandle->get(str)) != NULL)
				{
					strcpy(STC_offrdName, focus->GetName());
				}
			}
		}
		STC_offrdIsLocal = STC_isVesselLocal(STC_offrdName);
		return true;

	case OAPI_KEY_G:	// Go to Offer (here = offering client)
		if (!STC_ALLtrigger && !STC_offrdIsLocal) return true;
		if (STC_GOallowed) {
			tlogging(5, "STC/Offer: click on GIV");
			STC_offergInProcess = true;
			STC_clearToOffer = true;
			STC_offergWatchdog = 0.;
		}
		return STC_offergInProcess;

	case OAPI_KEY_B:	// Offer user's vessels to STC (here = offering client)
		STC_GOallowed = false;
		STC_ALLtrigger = false;
		STC_offergInProcess = false;
		STC_clearToOffer = true;
		STC_offergWatchdog = 0.;
		STC_takgWatchdog = 0.;
		STC_globalId = 0;
		if (focus != NULL) {
			sprintf(str, "%p", focus->GetHandle());
			if ((entry = (struct IDload*)clientData.GlobalsByHandle->get(str)) != NULL)
			{
				strcpy(STC_takenName, focus->GetName());
			}
		}
		STC_takgIsLocal = STC_isVesselLocal(STC_takenName);
		return true;

	case OAPI_KEY_T:	// Take back user's vessel, this cmd is a priority
		if (STC_ALLtrigger || STC_takgIsLocal) return true; // and do nothing (wrong click)
		STC_takingBack = !STC_takingBack;
		if (STC_takingBack) {
			tlogging(5, "STC/TkBck: click on TAK");
			STC_offergInProcess = false;
			STC_clearToOffer = false;
			STC_takgWatchdog = 0.;
			STC_globalId = 0;
			STC_GOallowed = false;
		}
		return true;
	}
return false;
}

//
bool STC::DataInput(void* id, char* str, void* data)
{
	return ((STC*)data)->DataInput(id, str);
}

bool STC::DataInput(void* id, char* str)
{
	tlogging(5, "STC: entering DataInput");
	switch (dialogSelection) {
	case SelectgSTC:
		sprintf(hostgName, "\"%s\"", str);
		// (checks here? if wrong, server will reply with ERR)
		break;
	//default: return true;
	}
	dialogSelection = 0;
	return true;
}

// ============================================================================
// MFD message parser
int STC::MsgProc(UINT msg, UINT mfd, WPARAM wparam, LPARAM lparam)
{
	switch (msg) {
	case OAPI_MSG_MFD_OPENED:
		// STC has been selected, so we create the MFD and
		// return a pointer to it.
		return (int)(new STC(LOWORD(wparam), HIWORD(wparam), (VESSEL*)lparam));
	}
	return 0;
}

/* ---------------
   Repaint the MFD
   ---------------  */
bool STC::Update(oapi::Sketchpad* skp)
{	// called at each step as long as the STC MFD is displayed
	char str[256] = "";
	char logLine[256] = "";
	VESSEL* focus = NULL;
	char focusName[99] = "";
	OBJHANDLE focobj = oapiGetFocusObject();
	if (oapiIsVessel(focobj)) focus = oapiGetVesselInterface(focobj);
	int yLine = 1;

	// Draws the MFD title
	Title(skp, "Space Traffic Center");
	
	yLine++;

	// General info: is the Vessel in control?
	skp->SetFont(font);
	skp->SetTextAlign(oapi::Sketchpad::CENTER, oapi::Sketchpad::BASELINE);
	if (focus == NULL) {
		skp->SetTextColor(0xFF0000);
		sprintf(str, "NOT IN A VESSEL");
		skp->Text(0.5 * W, yLine * (H / mxMFDlines), str, strlen(str));
		return false;
	}
	strcpy(focusName, focus->GetName());
	STC_offrdIsLocal = STC_isVesselLocal(focusName);
	// STC_takgIsLocal  = STC_isVesselLocal(focusName);
	if (STC_offrdIsLocal) {
		skp->SetTextColor(0x00FF00);
		sprintf(str, "%s is IN CONTROL", focusName);
		skp->Text(0.5 * W, yLine * (H / mxMFDlines), str, strlen(str));
		yLine++;
		if (focus->GetFlightStatus() == 1) sprintf(str, "-- GROUNDED --");
		else sprintf(str, "-- NOT grounded --");
		skp->Text(0.5 * W, yLine * (H / mxMFDlines), str, strlen(str));
		yLine++;
		sprintf(str, "offering %s to STC?", focusName);
		skp->Text(0.5 * W, yLine * (H / mxMFDlines), str, strlen(str));
	} else {
		skp->SetTextColor(0xFFFFFF);
		sprintf(str, "%s is NOT in control", focusName);
		skp->Text(0.5 * W, yLine * (H / mxMFDlines), str, strlen(str));
		yLine++;
		sprintf(str, "taking %s back?", focusName);
		skp->Text(0.5 * W, yLine * (H / mxMFDlines), str, strlen(str));
		yLine++;
	}

	yLine += 0.5;

	// STC name
	skp->SetFont(font);
	skp->SetTextColor(strlen(str) > 8 ? 0x00FF00:0x00FFFF);
	skp->SetTextAlign(oapi::Sketchpad::LEFT, oapi::Sketchpad::BASELINE);
	skp->Text(0.5*(W/mxMFDcols), yLine *(H/mxMFDlines), hostgName, strlen(hostgName));
	
	// ALL/ONE trigger
	skp->SetFont(font);
	skp->SetTextColor(0x00FF00);
	skp->SetTextAlign(oapi::Sketchpad::RIGHT, oapi::Sketchpad::BASELINE);
	if (STC_ALLtrigger) sprintf(str, "ALL");
	else sprintf(str, "(single)");
	skp->Text(W - 1 * (W / mxMFDcols), yLine *(H / mxMFDlines), str, strlen(str));

	yLine += 3.5;

	/* Parking / Unparking
	skp->SetFont(font);
	if (STC_offrdIsLocal) {
		int flightStatus = focus->GetFlightStatus();
		if (flightStatus == 1 || flightStatus == 3) {
			if (STC_unpark10sec) { sprintf(str, "Unpark?"); skp->SetTextColor(0x00FFFF); }
			else { sprintf(str, "unparking..."); skp->SetTextColor(0x00FF00); }
		} else {
			if (STC_unpark10sec) { sprintf(str, "parking..."); skp->SetTextColor(0x00FF00);}
			else { sprintf(str, "Park?");  skp->SetTextColor(0x00FFFF);}
		}
		skp->SetTextAlign(oapi::Sketchpad::LEFT, oapi::Sketchpad::BASELINE);
		skp->Text(0.5 * (W / mxMFDcols), yLine * (H / mxMFDlines), str, strlen(str));
	}*/

	yLine += 3;
	yLine += 3;
	yLine += 3;

	// OFR selection
	skp->SetFont(font);
	if (STC_ALLtrigger) { sprintf(str, "OFR ALL ?"); skp->SetTextColor(0x00FFFF); }
	else {
		if (!STC_offrdIsLocal) sprintf(str, "");
		else sprintf(str, "OFR \"%s\"?", STC_offrdName); skp->SetTextColor(0x00FF00);
	}
	skp->SetTextAlign(oapi::Sketchpad::LEFT, oapi::Sketchpad::BASELINE);
	skp->Text(0.5*(W/mxMFDcols), yLine * (H/mxMFDlines), str, strlen(str));

	// TAK: vessel to take back
	skp->SetFont(font);
	*str = 0x00;
	if (!STC_takgIsLocal && !STC_ALLtrigger) {
		sprintf(str, "BCK \"%s\"?", STC_takenName);
	} else if (STC_takgIsLocal) sprintf(str, "");
	skp->SetTextColor(0x00FF00);
	skp->SetTextAlign(oapi::Sketchpad::RIGHT, oapi::Sketchpad::BASELINE);
	skp->Text(W - 1 * (W / mxMFDcols), yLine * (H / mxMFDlines), str, strlen(str));

	yLine += 3;

	// GO button
	skp->SetFont(fontB);
	skp->SetTextAlign(oapi::Sketchpad::LEFT, oapi::Sketchpad::BASELINE);
	if (STC_GOallowed && (STC_offrdIsLocal || STC_ALLtrigger) && !STC_offergInProcess) {
		skp->SetTextColor(0x00FF00);
		skp->Text(0.5 * (W / mxMFDcols), yLine * (H / mxMFDlines), " <<<<<", 6);
	} else if (STC_offergInProcess) {
		skp->SetTextColor(0x00FFFF);
		skp->Text(0.5 * (W / mxMFDcols), yLine * (H / mxMFDlines), "(processg)", 10);
	} else if (STC_takingBack) {
		skp->SetTextColor(0x0000FF);
		skp->Text(0.5 * (W / mxMFDcols), yLine * (H / mxMFDlines), "(busy)", 6);
	}

	// BCK button
	skp->SetFont(fontB);
	skp->SetTextAlign(oapi::Sketchpad::RIGHT, oapi::Sketchpad::BASELINE);
	if (STC_takingBack) {
		skp->SetTextColor(0x00FFFF);
		skp->Text(W - 1 * (W / mxMFDcols), yLine * (H / mxMFDlines), "(processg)", 10);
	}
	else if (!STC_takgIsLocal && !STC_ALLtrigger) {
		skp->SetTextColor(0x00FF00);
		skp->Text(W - 1 * (W / mxMFDcols), yLine* (H / mxMFDlines), ">>>>> ", 6);
	}

	// Add MFD display routines here.
	// Use the device context (hDC) for Windows GDI paint functions.
	return true;
}

