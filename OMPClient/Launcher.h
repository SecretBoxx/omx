#pragma once
#pragma unmanaged
#include "orbitersdk.h"
#pragma managed
#include <vcclr.h>

using namespace System::Reflection;

namespace Orbiter
{
	namespace Multiplayer
	{
		namespace Client
		{
			public ref class Launcher
			{
			public:
				Launcher();
				void Start();
				void Stop();
				void Update();
				void Propagate();
				void Add(char *message);
				void Up();
				void Down();
				void GenerateSNTPLog();
				bool CheckSNTPServer();
				System::IntPtr GetNewWindow();
				void SaveConfig();
				void Log(int level, char *message);
				void Analyse();
				void PTPSample(double t1, double t2, double t3, double t4);
				int CompareVersions(char *v1, char *v2);
				virtual property double Now{double get(void);};
				virtual property double NowLocal{double get(void);};
			private:
				Object^ managed;
				MethodInfo^ start;
				MethodInfo^ stop;
				MethodInfo^ update;
				MethodInfo^ propagate;
				MethodInfo^ add;
				MethodInfo^ up;
				MethodInfo^ down;
				MethodInfo^ generateSNTPLog;
				MethodInfo^ getNewWindow;
				MethodInfo^ saveConfig;
				MethodInfo^ log;
				MethodInfo^ analyse;
				MethodInfo^ ptpSample;
				MethodInfo^ compareVersions;
				PropertyInfo^ now;
				PropertyInfo^ nowLocal;
				PropertyInfo^ synchronizationAverage;
				PropertyInfo^ synchronizationMaximum;
				PropertyInfo^ synchronizationMinimum;
				PropertyInfo^ clockVisibility;
				PropertyInfo^ clockPulseVisibility;
				PropertyInfo^ sampleWaitTime;
				PropertyInfo^ synchronizationWaitTime;
				PropertyInfo^ logger;
				PropertyInfo^ mjdAverage;
				PropertyInfo^ mjdMaximum;
				PropertyInfo^ mjdMinimum;
				PropertyInfo^ systemVisibility;
				PropertyInfo^ pulse;
				PropertyInfo^ name;
				PropertyInfo^ password;
				PropertyInfo^ ip;
				PropertyInfo^ tcp;
				PropertyInfo^ udp;
				PropertyInfo^ method;
				PropertyInfo^ scenario;
				PropertyInfo^ container;
				PropertyInfo^ quickLaunch;
				PropertyInfo^ quickLeave;
			};

			public class Wrapper
			{
			public:
				Wrapper(Launcher^ launcher);
				void Start();
				void Stop();
				void Update();
				void Propagate();
				void Add(char *message);
				void Up();
				void Down();
				void GenerateSNTPLog();
				bool CheckSNTPServer();
				HWND GetNewWindow();
				void SaveConfig();
				void Log(int level, char *message);
				void Analyse();
				void PTPSample(double t1, double t2, double t3, double t4);
				double Now(bool compensated);
				/// <summary>
				/// Compares the version strings.
				/// </summary>
				/// <param name="v1">The first version.</param>
				/// <param name="v2">The second version.</param>
				/// <returns>
				/// -1 if the first version is greater than the second,
				///  0 if equal,
				///  1 if the first version is less than the second,
				/// </returns>
				int CompareVersions(char *v1, char *v2);
			private:
				gcroot<Launcher^> launcher;
			};
		}	
	}
}
