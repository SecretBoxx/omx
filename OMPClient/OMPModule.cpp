#pragma unmanaged
#include "OMPModule.h"

extern void Init(HINSTANCE hDLL);
extern void Exit(HINSTANCE hDLL);
//extern void OpenRenderViewport (HWND hWnd, oapi::Module::RenderMode mode);
extern void CloseRenderViewport ();
extern void PreStep (double simt, double simdt, double mjd);
extern void PostStep (double simt, double simdt, double mjd);
extern void AddVessel(OBJHANDLE obj);
extern void DeleteVessel(OBJHANDLE obj);

OMPModule::OMPModule(HINSTANCE hDLL):Module(hDLL)
{
	this->hDLL=hDLL;
	Init(hDLL);
}

OMPModule::~OMPModule(void)
{
	Exit(hDLL);	
}

void OMPModule::clbkNewVessel(OBJHANDLE hVessel)
{
	AddVessel(hVessel);
}

void OMPModule::clbkDeleteVessel(OBJHANDLE hVessel)
{
	DeleteVessel(hVessel);
}

void OMPModule::clbkPreStep(double simt, double simdt, double mjd)
{
	PreStep(simt, simdt, mjd);
}

void OMPModule::clbkPostStep(double simt, double simdt, double mjd)
{
	PostStep(simt, simdt, mjd);
}

/*void OMPModule::clbkSimulationStart(oapi::Module::RenderMode mode)
{
	OpenRenderViewport(hWnd, mode);
}*/

void OMPModule::clbkSimulationEnd()
{
	CloseRenderViewport();
}