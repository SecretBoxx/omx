// ==============================================================
//              "Space Traffic Control" MFD for OMX
// to transfer a vessel's state vector between two OMP Clients
//
//   (c) 11/2022-2024+ Boris Segret, due to OMP, GPL license applies
// 
// MFDTemplate.cpp from ORBITER SDK, was a starting point for this.
// ORBITER SDK, (C) 2003-2016 Martin Schweiger, under MIT license
// 
// STC MFD has been integrated to OMX (no longer independant MFD).
// OMX is an evolution of OMP (Orbiter Multiplayer Project), great
// work by Friedrich Kastner - Masilko, (C) 2007 under GPL.
// 
// More contributiors in GPLv2 from Orbiter-Forum.com community:
// "asbjos" & "jarmonik" (STC_LandIt)
// ==============================================================

#ifndef STC_C2C_H
#define STC_C2C_H

#define STRICT
#define ORBITER_MODULE

#include "windows.h"
#include "orbitersdk.h"
#include "OMPModule.h"
#include "HashTable.h"
// common definitions/declartations with OMPClient.cpp, STC_*.cpp, OMX_backup*.cpp:
#include "..\OMPClient\STC_BNR.h"

 /*\                              /*\
<   >----- Global variables -----<   >
 \_/                              \*/

int g_STCmode; // identifier for new MFD mode
#define SelectgSTC 1
#define mxMFDlines 21
#define mxMFDcols 40

bool STC_Registered = false; // user is registered or not

// global types (taken-over from OMPClient.cpp)
#define LINESIZE		4096
void tlogging(int level, char* text);
struct payload
{
	char* nick, * passwd;
	unsigned long ip_addr;
	int receiverport, sendsocket, sendtoport;
	double timestamp;
	//Hashtables
	CHashTable* GlobalsByID;
	CHashTable* GlobalsByHandle;
	CHashTable* LocalsByID;
	unsigned short int LocalID;
	CHashTable* Neighbours;
	struct MessageLoad* MessageQueue;
	struct KillLoad* KillList;
};
struct payload		clientData;

struct IDload
{
	unsigned short int	ID;
	unsigned short int	refcount;
	int					MasterID;
	IDload* GlobalID;
	OBJHANDLE			object;
	VESSEL* vessel;
	char				size;
	char				linkage;	// ==ascii(1) if locally controlled vessel
	double				tsSent;
	double				savedMJD = 0.; // local backup for local vessels
	char* NavInfo;
	bool  Landed;
	unsigned short int LandBody = 0; // celestial body, if landed
	double LandInfo[3] = { 0,0,0 }; // long.,lat.,heading if Landed
	char* DockInfo;
	union
	{
		//for local vessels
		struct
		{
			unsigned short int	MasterPreset;
			unsigned char		justAdded;
			int					idle;
			double				lastJump;
		};
		//for remote vessels
		struct
		{
			THRUSTER_HANDLE		thruster;
			PROPELLANT_HANDLE	fuel;
			double				lastInfoPing;
		};
	};
};

// regarding the offer/accept processes:
bool STC_GOallowed = true;			// turns on/off the "GO" button
bool STC_offergInProcess = false;	// states or stops a single offer process
bool STC_clearToOffer = true;		// allows a single offer process (and no other running)
double STC_offergWatchdog = 0.;
int STC_globalId = 0;
bool STC_offrdIsLocal = false;
char hostgName[256] = "\"ORBIX\"";
char STC_offrdName[99] = "NN-iii";
int STC_nxtLocal = -1;
bool STC_clearToAccpt = true;

// other global variables
int STC_debugNbLocal = 0;
char STC_ghostName[99] = "(ghost)";
OBJHANDLE STC_ghostH = NULL;
bool STC_ALLtrigger = false;
bool STC_takingBack = false;
bool STC_takgIsLocal = false;
bool STC_givenBack = true;
char STC_takenName[99] = "NN-iii";
double STC_takgWatchdog = 0.;
int dialogSelection = 0;

// ==============================================================
// DELTAGLIDER properties (hence, limited to DeltaGlider, at the moment)

static const DWORD ntdvtx_gearup = 13;
static TOUCHDOWNVTX tdvtx_gearup[ntdvtx_gearup] = {
	{_V(0   ,-1.5 ,9),     1e7, 1e5, 3.0, 3.0},
	{_V(-6   ,-0.8 ,-5),    1e7, 1e5, 3.0, 3.0},
	{_V(3   ,-1.2 ,-5),    1e7, 1e5, 3.0, 3.0},
	{_V(-8.5 ,-0.3 ,-7.05), 1e7, 1e5, 3.0},
	{_V(8.5 ,-0.3 ,-7.05), 1e7, 1e5, 3.0},
	{_V(-8.5 ,-0.4 ,-3), 1e7, 1e5, 3.0},
	{_V(8.5 ,-0.4 ,-3), 1e7, 1e5, 3.0},
	{_V(-8.85, 2.3 ,-5.05), 1e7, 1e5, 3.0},
	{_V(8.85, 2.3 ,-5.05), 1e7, 1e5, 3.0},
	{_V(-8.85, 2.3 ,-7.05), 1e7, 1e5, 3.0},
	{_V(8.85, 2.3 ,-7.05), 1e7, 1e5, 3.0},
	{_V(0   , 2   , 6.2), 1e7, 1e5, 3.0},
	{_V(0   ,-0.6 ,10.65), 1e7, 1e5, 3.0}
};

static const DWORD ntdvtx_geardown = 13;
static TOUCHDOWNVTX tdvtx_geardown[ntdvtx_geardown] = {
	{_V(0   ,-2.57,10), 1e6, 1e5, 1.6, 0.1},
	{_V(-3.5 ,-2.57,-1), 1e6, 1e5, 3.0, 0.2},
	{_V(3.5 ,-2.57,-1), 1e6, 1e5, 3.0, 0.2},
	{_V(-8.5 ,-0.3 ,-7.05), 1e7, 1e5, 3.0},
	{_V(8.5 ,-0.3 ,-7.05), 1e7, 1e5, 3.0},
	{_V(-8.5 ,-0.4 ,-3), 1e7, 1e5, 3.0},
	{_V(8.5 ,-0.4 ,-3), 1e7, 1e5, 3.0},
	{_V(-8.85, 2.3 ,-5.05), 1e7, 1e5, 3.0},
	{_V(8.85, 2.3 ,-5.05), 1e7, 1e5, 3.0},
	{_V(-8.85, 2.3 ,-7.05), 1e7, 1e5, 3.0},
	{_V(8.85, 2.3 ,-7.05), 1e7, 1e5, 3.0},
	{_V(0   , 2.0   , 6.2), 1e7, 1e5, 3.0},
	{_V(0   ,-0.6 ,10.65), 1e7, 1e5, 3.0}
};

// ==============================================================
// Local prototypes and their globals

bool STC_isVesselLocal(char* vesselName);
bool STC_isVesselLocal(OBJHANDLE hVessel);
bool STC_isVesselLocal(int gID);
bool STC_OfferVessels(double simTime);
bool STC_TakbackVessels(double simTime);
void STC_UpdateState(IDload* entry);
void STC_RestoreState(IDload* entry, bool deleteAfter);
void STC_OwnVesselLanded(OBJHANDLE handle);
void STC_LandIt(VESSEL* v, OBJHANDLE rbody, double longitude, double latitude, double heading);

double STC_dtBackup = 3. / 86400.; // interval between local backup [day]
bool STC_bkFullState(IDload* entry, VESSELSTATUS2* vs, UDPpacket* pac);
bool STC_rsFullState(char* callSign);
DWORD  STC_backupPID = NULL; // see STC_Vessels.cpp for STC_keepBackupAlive();
const std::chrono::seconds oneSec{ 1 };
std::filesystem::file_time_type OMX_backupCheck;
void STC_keepBackupAlive();

bool STC_restoring = false;
double STC_nextMJDrestore = -1;
std::string STC_nextFileRestore = "";
enum STC_restore {none, init, next};
STC_restore STC_progress = none;
int STC_restoreFromFolder();

class STC: public MFD2 {
public:
	STC(DWORD w, DWORD h, VESSEL *vessel);
	~STC();
	char *ButtonLabel (int bt);
	int ButtonMenu (const MFDBUTTONMENU **menu) const;
	bool Update (oapi::Sketchpad *skp);
	static int MsgProc (UINT msg, UINT mfd, WPARAM wparam, LPARAM lparam);
	//void clbkPreStep(double SimT, double SimDT, double mjd);

private:
	bool ConsumeKeyBuffered(DWORD key);
	bool ConsumeButton(int bt, int event);
	static bool DataInput(void* id, char* str, void* data);
	bool DataInput(void* id, char* str);

protected:
	int n = 0;
	oapi::Font* font;
	oapi::Font* fontB;
};


#endif // !STC_C2C_H