// ==============================================================
//              "Space Traffic Control" MFD for OMX
// to transfer a vessel's state vector between two OMP Clients
//
//   (c) 11/2022-2024+ Boris Segret, due to OMP, GPL license applies
// 
// MFDTemplate.cpp from ORBITER SDK, was a starting point for this.
// ORBITER SDK, (C) 2003-2016 Martin Schweiger, under MIT license
// 
// STC MFD has been integrated to OMX (no longer independant MFD).
// OMX is an evolution of OMP (Orbiter Multiplayer Project), great
// work by Friedrich Kastner - Masilko, (C) 2007 under GPL.
// 
// More contributiors in GPLv2 from Orbiter-Forum.com community:
// "asbjos" & "jarmonik" (STC_LandIt)
// ==============================================================

#include "STC_C2C.h"

#include "STC_GUI.cpp"
#include "STC_Vessels.cpp"
#include "STC_Moves.cpp"
