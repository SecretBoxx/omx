// ==============================================================
//              "Space Traffic Control" MFD for OMX
// part of STC
//   (c) 11/2022-2024+ Boris Segret, due to OMP, GPL license applies
// included:
//   bool STC_isVesselLocal(char* vesselName)
//   bool STC_isVesselLocal(OBJHANDLE hVessel)
//   bool STC_isVesselLocal(int gID)
//   void STC_UpdateState(IDload* entry)
//   void STC_RestoreState(IDload* entry, bool deleteAfter)
//   bool STC_bkFullState(IDload* entry, VESSELSTATUS2* vs, UDPpacket* pk)
//   bool STC_rsFullState(IDload* entry)
//   void STC_restoreFromFolder();
//   void STC_keepBackupAlive();
// ==============================================================

/// <summary>
/// Checks whether vesselName corresponds to a locally created/managed vessel
/// </summary>
/// <param name="vesselName">name of a vessel (should be unique)</param>
/// <returns></returns>
bool STC_isVesselLocal(char* vesselName)
{
	OBJHANDLE hVessel;
	if ((hVessel = oapiGetVesselByName(vesselName)) == NULL) return false;
	return STC_isVesselLocal(hVessel);
}


/// <summary>
/// Checks whether hVessel corresponds to a locally created/managed vessel
/// </summary>
/// <param name="hVessel">handle to the vessel</param>
/// <returns></returns>
bool STC_isVesselLocal(OBJHANDLE hVessel)
{
	IDload* entry = NULL;
	CHashData *entryHash = NULL;
	char line[LINESIZE], msg[LINESIZE];
	int nbOfLocals = 0;

	if (clientData.LocalsByID == 0) return false;
	sprintf(line, "%p", hVessel);
	if ((entry = (struct IDload*)clientData.GlobalsByHandle->get(line)) == NULL) return false;
	//sprintf(msg, "STC_isVesselLocal/handle vH%s, linkage=0x%02X", line, entry->linkage);
	nbOfLocals = clientData.LocalsByID->get_count();

	for (unsigned long int bucket=0;
		bucket < clientData.LocalsByID->get_size();
		bucket++)
	{
		if ((entryHash = clientData.LocalsByID->content(bucket)) == NULL) continue;
		if ((entry = (struct IDload*)entryHash->data) == NULL) continue;
		if (entry->object == hVessel) {
			//sprintf(line, "%s = [L%d](bucket #%d), IS local, among %d locals", msg, entry->ID, bucket, nbOfLocals); tlogging(5, line);
			return true;
		}
	}
	//sprintf(line, "%s is NOT local, among %d locals", msg, nbOfLocals); tlogging(5, line);
	return false;
}

/// <summary>
/// Checks whether global ID gID corresponds to a locally created/managed vessel
/// </summary>
/// <param name="gID">Global ID (allocated by OMX server)</param>
/// <returns></returns>
bool STC_isVesselLocal(int gID)
{
	IDload* entry;
	char line[20];
	if (clientData.LocalsByID == 0) return false;
	sprintf(line, "%d", gID);
	if ((entry = (struct IDload*)clientData.GlobalsByID->get(line)) == NULL) return false;
	return STC_isVesselLocal(entry->object);
}

/// <summary>
/// Creates an updated temporary file in Server\Data for the given entry (local/remote vessel)
/// </summary>
/// <param name="entry">entry points to the properties of a locally or remotely managed vessel</param>
void STC_UpdateState(IDload* entry)
{
	char SNDName[10] = "SND456789";
	char fileName[100];
	//char trckFile[10] = "TKS456789";
	char line[BUFFERSIZE], * quotebreak, debugMsg[LINESIZE];
	int gID = entry->ID;

	itoa(gID, SNDName + 3, 10);
	sprintf(fileName, "%s%s", BKP_PATH.c_str(), SNDName);
	//itoa(gID, trckFile + 3, 10);
	sprintf(debugMsg, "STC_UpdateState: updating [G%d]->%s", gID, fileName);
	//tlogging(5, debugMsg);

	//
	// Saving the current state in a local file
	// 
	// COMMON PROCESS
	FILEHANDLE stateFile=oapiOpenFile(fileName, FileAccessMode::FILE_OUT, PathRoot::MODULES);
	
	//entry->vessel->SaveDefaultState(stateFile);
	sprintf(line, "STATUS %s", entry->Landed?"Landed":"Flying");
	if (entry->Landed)
		sprintf(line, "%s %5d %12.9f %12.9f %12.9f", line, entry->LandBody, entry->LandInfo[0],entry->LandInfo[1],entry->LandInfo[2]);
	oapiWriteLine(stateFile, line);

	// OMX: EXTENDED state vector if STC_offergInProcess and current vessel being offered
	// this routine is run as "Client A" (offering client)
	if (STC_offergInProcess) if (gID == STC_globalId) {
		sprintf(debugMsg, "%s, running OFFER for \"%s\", extended status", debugMsg, STC_offrdName);
		//tlogging(5, debugMsg);
		for (int matchi = entry->vessel->GetPropellantCount() - 1; matchi >= 0; matchi--)
		{
			sprintf(line, "$XF%d %f %f %f",
				matchi,
				entry->vessel->GetPropellantEfficiency(entry->vessel->GetPropellantHandleByIndex(matchi)),
				entry->vessel->GetPropellantMaxMass(entry->vessel->GetPropellantHandleByIndex(matchi)),
				entry->vessel->GetPropellantMass(entry->vessel->GetPropellantHandleByIndex(matchi)));
			oapiWriteLine(stateFile, line);
		}
		for (int matchi = entry->vessel->GetThrusterCount() - 1; matchi >= 0; matchi--)
		{
			sprintf(line, "$XT%d %f %f",
				matchi,
				entry->vessel->GetThrusterMax0(entry->vessel->GetThrusterHandleByIndex(matchi)),
				entry->vessel->GetThrusterLevel(entry->vessel->GetThrusterHandleByIndex(matchi)));
			oapiWriteLine(stateFile, line);
		}

		// then, also creates a "(ghost)" with the vessel's state
		bool exselfCreate = selfCreate;
		VESSELSTATUS2 ghoststate;
		VESSEL* ghostV;
		OBJHANDLE entryHandle = entry->vessel->GetHandle(), ghostH;
		MATRIX3 temprot;
		VECTOR3 deltaV = unit(_V(0, 0.5, -0.866)); // -1 m/s, 30 deg top
		VECTOR3 absDeltaV;

		ghoststate = dinfo.snapshot[entry->object];
		ghoststate.version = 2; // for safety, due to DefSetStateEx later
		entry->vessel->GetRotationMatrix(temprot);
		RotateVector(deltaV, temprot, absDeltaV);
		ghoststate.rvel += absDeltaV;
		ghoststate.vrot = _V(0, 0, 0);

		if ((ghostH = oapiGetVesselByName(STC_ghostName)) == NULL) {
			selfCreate = true;
			ghostH = oapiCreateVesselEx(STC_ghostName, "OMPDefault", &ghoststate);
			STC_ghostH = ghostH;
			selfCreate = exselfCreate;
			sprintf(debugMsg, "%s, creating %s", debugMsg, STC_ghostName);
			//tlogging(5, debugMsg);
		} else sprintf(debugMsg, "%s, re-using %s", debugMsg, STC_ghostName);
		ghostV = oapiGetVesselInterface(ghostH);
		ghostV->DefSetStateEx(&ghoststate);
		if (entry->Landed)
			STC_LandIt(ghostV, ghoststate.rbody, entry->LandInfo[0], entry->LandInfo[1], entry->LandInfo[2]);

		if (entryHandle == oapiGetFocusObject()) {
			//oapiSetFocusObject(ghostH);
			oapiCameraAttach(ghostH, 1);
			sprintf(debugMsg, "%s + set focus", debugMsg);
		} // does nothing if the focus is NOT on the transferred vessel (e.g. TKBCK)
	}

	// (TODO) DELTAGLIDER-specific properties (hence, limited to DeltaGlider, at the moment)
	char* classname = "DeltaGlider";
	if (strcmp(classname, entry->vessel->GetClassNameA()) == 0) {	}
	
	// also updating animations (COMMON PROCESS)
	strcpy(line, OMPSTATELABEL);
	ANIMATION* anim;
	UINT anims = entry->vessel->GetAnimPtr(&anim);
	for (UINT i = 0; i < anims; i++)
	{   // e.g.: 9A9999999999D93F 9A9999999999D93F 0 0 0 0 E03F F03F F03F F03F 0 0 0 0 0 0
		if ((i % MAXANIMPERLINE) == 0)
		{
			oapiWriteLine(stateFile, line);
			sprintf(line, "%X", i);
			quotebreak = line + strlen(line);
		}
		//Minimum double value serializing: inverse byte order, so full numbers have leading zeros, which will be skipped
		*quotebreak = ' ';
		byte* s = (byte*)(void*)&(anim[i].state); // just creates a variable with the right size
		for (int j = 0; j < 8; j++)
		{
			if (s[j] == 0 && j < 7 && *quotebreak == ' ') continue;
			byte u = s[j] >> 4;
			if (u != 0 || *quotebreak != ' ') *(++quotebreak) = u > 9 ? 'A' + u - 10 : '0' + u;
			u = s[j] & 0x0F;
			*(++quotebreak) = u > 9 ? 'A' + u - 10 : '0' + u;
		}
		*(++quotebreak) = 0x00;
	}
	oapiWriteLine(stateFile, line);

	oapiCloseFile(stateFile, FileAccessMode::FILE_OUT);

	//
	// sending up-to-date GINFO to server via TCP
	//
	char pingbuf[BUFFERSIZE];
	char ginfo[13];
	sprintf(ginfo, "GINFO %d", gID);
	sprintf(pingbuf, "%s\"C%s\"%s", ginfo,
			entry->vessel->GetName(),
		entry->vessel->GetClassNameA());
	int matchi;
	int bufferOffset = 0;
	bool ostate = false;
	stateFile = oapiOpenFile(fileName, FileAccessMode::FILE_IN, PathRoot::MODULES);
	int sendState = 1;
	for (; sendState != 0; sendState++)
	{
		sprintf(line, "--> %s", pingbuf);
		tlogging(5, line);
		int match = (int)dinfo.shsfxasc[0]; //Pascal-Type String (First character=length)
		int buflen = strlen(pingbuf);
		//do not insert a NULL-character due to it's string terminating nature in C++
		//instead such, insert a 0x03-character (end of text)
		for (matchi = 1; matchi <= match; matchi++)
			if ((pingbuf[buflen - 1 + matchi] = dinfo.shsfxasc[matchi]) == 0x00) pingbuf[buflen - 1 + matchi] = 0x03;
		//add a string termination
		pingbuf[buflen - 1 + matchi] = 0x00;
		EnterCriticalSection(&(xthreaddata->mutex));
		// the current content of pingbuf is sent.
		if (send(xthreaddata->com, pingbuf, strlen(pingbuf), 0) == SOCKET_ERROR)
		{
			sprintf(line, "Error sending via TCP %d to server:%d", xthreaddata->com, ntohs(xthreaddata->remote_addr.sin_port));
			tlogging(20, line);
		}
		LeaveCriticalSection(&(xthreaddata->mutex));
		// if more data is to be sent, the beginning of pingbuf is re-used,
		// the content after bufferOffset is given by lines from stateFile
		if (sendState > 0)
		{
			char* oapiLine;
			if (oapiReadScenario_nextline(stateFile, oapiLine))
			{
				if (strcmp(oapiLine, OMPSTATELABEL) == 0)
				{
					ostate = true;
					sprintf(pingbuf, "%s:-", ginfo);
				}
				else if (oapiLine[0] == '$') sprintf(pingbuf, "%s:%s", ginfo, oapiLine);
				else sprintf(pingbuf, "%s:%c%s", ginfo, ostate ? '^' : '|', oapiLine);
				// pingbuf will be sent at the next "for" loop
			}
			else
			{
				oapiCloseFile(stateFile, FileAccessMode::FILE_IN);
				//CopyFile(fileName, trckFile, false); // <===== DEBUG need
				DeleteFile(fileName);
				sendState = -1; // then will become 0 at the exit of the "for" loop, hence terminating the loop (sooo, complicated!)
			}
		}
	}
	sprintf(debugMsg, "%s, sent!", debugMsg); tlogging(5, debugMsg);
}

/// <summary>
/// Runs because GINFO case '-' was received (lines with "^" will be interpreted afterwards
/// in OMPClient.cpp). Hence, <fileName> shall only contain "|" and "$" lines from GINFO.
/// Lines "$" are for vessel transfer, to be interpreted by "Client B" (accepting client)
/// if B's STC_clearToAccpt == false and B's STC_globalId > 0. Then, B's STC_clearToAccpt
/// is turned true.
/// </summary>
/// <param name="entry">entry to data that need to be re-created locally</param>
/// <param name="deleteAfter">true/false, the file must be deleted (true) after ACCPT </param>
void STC_RestoreState(IDload* entry, bool deleteAfter)
{
	char RCVName[10] = "RCV456789";
	//char trckFile[10] = "TKS456789";
	char fileName[100];
	FILEHANDLE stateFile;
	char* oapiLine;
	char line[LINESIZE], key[100];
	bool extendedStatusReceived = false;
	int nItem;
	double maxValue, currentValue, efficiency;
	VESSELSTATUS2 status = dinfo.snapshot[entry->object]; // only used for unrecognized lines in file fileName
	// declaring more variable for ParkingBrake status:
	//VESSELSTATUS2 vs;
	bool justLanded = false, recreation = false;
	IDload* pBody;
	OBJHANDLE hPbody;
	double longitude, latitude;
	double heading = 0.0;
	int gBody;

	itoa(entry->ID, RCVName + 3, 10);
	sprintf(fileName, "%s%s", BKP_PATH.c_str(), RCVName);
	//itoa(entry->ID, trckFile + 3, 10);
	if ((stateFile = oapiOpenFile(fileName, FileAccessMode::FILE_IN_ZEROONFAIL, PathRoot::MODULES)) == 0)
	{	// the file doesn't exist: ID is likely a local ID if just recreated from startmove
		if (!STC_clearToAccpt && entry->ID == STC_nxtLocal && entry->linkage == 1) {
			itoa(STC_globalId, RCVName + 3, 10);
			sprintf(fileName, "%s%s", BKP_PATH.c_str(), RCVName);
			//itoa(STC_globalId, trckFile + 3, 10);
			if ((stateFile = oapiOpenFile(fileName, FileAccessMode::FILE_IN_ZEROONFAIL, PathRoot::MODULES))
				== 0) {
				sprintf(line, "STC_RestoreState [G%d] cannot open file \"%s\"", STC_globalId, fileName);
				tlogging(20, line);
				return;
			}
		}
		else {
			sprintf(line, "STC_RestoreState [G%d] cannot open file \"%s\"", entry->ID, fileName);
			tlogging(20, line);
			return;
		}
	}
	sprintf(line, "STC_RestoreState [G/L%d], then delete? %s", entry->ID, deleteAfter?"yes":"no");
	//tlogging(5, line);

	// Expected first line of <statefile> is STATUS
	//CopyFile(fileName, trckFile, false); // <===== DEBUG need (trckFile is saved at Orbiter's root)
	oapiReadScenario_nextline(stateFile, oapiLine);
	// e.g.
	//   STATUS Landed Earth
	// 	 POS -52.3397127 4.8146463
	// 	 HEADING 80.00
	// reformated into:
	//   STATUS Landed <gBody_ID> <Long> <Lat> <Headg>
	if (!strnicmp(oapiLine, "STATUS", 6)) {
		if ((!strnicmp(oapiLine + 7, "Landed", 6))) {
			if (!entry->Landed) justLanded = true;
			sprintf(line, "%s, Landed", line); //tlogging(5, line);
			if (sscanf(oapiLine + 14, "%i %lf %lf %lf", &gBody, &longitude, &latitude, &heading) != 4) {
				sprintf(line, "%s, format ERROR in %s", line, oapiLine + 14);
				tlogging(20, line);
				return;
			}
			sprintf(key, "%d", gBody);
			if ((pBody = (struct IDload*)clientData.GlobalsByID->get(key)) == NULL) {
				sprintf(line, "%s, unknown grav.body ID_%d", line, gBody);
				tlogging(20, line);
				return;
			}
			else hPbody = pBody->object;
		}
		else entry->Landed = false; // resets the use of SI packets
	}
	else {
		sprintf(line, "%s, %s not starting with STATUS line!", line, fileName);
		tlogging(20, line);
		return;
	}

	if (!STC_clearToAccpt && entry->ID == STC_nxtLocal && entry->linkage==1) {
		// an accepting process is running AND this vessel is the one transferred
		sprintf(line, "%s, Extended GINFO", line);
		while (oapiReadScenario_nextline(stateFile, oapiLine)) {
			if (!strnicmp(oapiLine, "$XF", 3)) {
				extendedStatusReceived = true;
				/*sprintf(line, "$XF%d %f %f %f",
					matchi,
					entry->vessel->GetPropellantEfficiency(entry->vessel->GetPropellantHandleByIndex(matchi)),
					entry->vessel->GetPropellantMaxMass(entry->vessel->GetPropellantHandleByIndex(matchi)),
					entry->vessel->GetPropellantMass(entry->vessel->GetPropellantHandleByIndex(matchi)));					*/
				if (sscanf(oapiLine + 3, "%d %lf %lf %lf", &nItem, &efficiency, &maxValue, &currentValue) != 4) {
					sprintf(line, "%s, format ERROR in Fu.#%d: eff %f, max %f current %f", line, nItem, efficiency, maxValue, currentValue);
					tlogging(20, line);
				}
				else {
					sprintf(line, "%s, %s", line, oapiLine);
				}
				entry->vessel->SetPropellantEfficiency(entry->vessel->GetPropellantHandleByIndex(nItem), efficiency);
				entry->vessel->SetPropellantMaxMass(entry->vessel->GetPropellantHandleByIndex(nItem), maxValue);
				entry->vessel->SetPropellantMass(entry->vessel->GetPropellantHandleByIndex(nItem), currentValue);
			}
			else if (!strnicmp(oapiLine, "$XT", 3)) {
				extendedStatusReceived = true;
				/*sprintf(line, "$XT%d %f %f",
					matchi,
					entry->vessel->GetThrusterMax0(entry->vessel->GetThrusterHandleByIndex(matchi)),
					entry->vessel->GetThrusterLevel(entry->vessel->GetThrusterHandleByIndex(matchi))); */
				if (sscanf(oapiLine + 3, "%d %lf %lf", &nItem, &maxValue, &currentValue) != 3) {
					sprintf(line, "%s, format ERROR in Th.#%d: max %f current %f", line, nItem, maxValue, currentValue);
					tlogging(20, line);
				}
				entry->vessel->SetThrusterMax0(entry->vessel->GetThrusterHandleByIndex(nItem), maxValue);
				entry->vessel->SetThrusterLevel(entry->vessel->GetThrusterHandleByIndex(nItem), currentValue);
			}
			else entry->vessel->ParseScenarioLineEx(oapiLine+1, &status);  // anything not recognised is passed on to Orbiter
		}
		if (extendedStatusReceived) sprintf(line, "%s (found)", line);
		else sprintf(line, "%s (NOT found!)", line);
	}
	else {
		sprintf(line, "%s, standard", line);
		// reads the rest of the GINFO (before "-")
		while (oapiReadScenario_nextline(stateFile, oapiLine)) entry->vessel->ParseScenarioLineEx(oapiLine + 1, &status);
	}

	if (justLanded) {
		STC_LandIt(entry->vessel, hPbody, longitude, latitude, heading);
		entry->Landed = true;
		entry->LandBody = (unsigned short)gBody;
		entry->LandInfo[0] = longitude;
		entry->LandInfo[1] = latitude;
		entry->LandInfo[2] = heading;
	}

	sprintf(line, "%s, done.", line);
	tlogging(5, line);

	oapiCloseFile(stateFile, FileAccessMode::FILE_IN);
	if (deleteAfter) {
		DeleteFile(fileName);
		//sprintf(line, "STC_RestoreState: deleted %s", fileName); tlogging(5, line);
	}
	return;
}

// for next 2 routines: STC_bkFullState, STC_rsFullState
#include <fstream>
#define BUFFER_SIZE 4*1024		// max size of a backup file (compatible with all TCP implementations)
#define BKPREFIX "0"			// prefix for backup files just-written
#define PREFIX "8"				// prefix for backup files to-be-restored

/// <summary>
/// Called by OMPClient.cpp/hostsendthread for 1 local registered vessel <entry> periodically. 
/// Backs up one vessel's extended state, <entry>, into a binary file under Server\Data\. The 
/// just-prepared UDP packet is re-used. The updated file is named "0[callsign].data".
/// </summary>
/// <param name="entry">properties of a locally managed vessel</param>
/// <param name="vs">vessel's status</param>
/// <param name="pk">UDP package content ready to be sent</param>
/// <returns></returns>
bool STC_bkFullState(IDload* entry, VESSELSTATUS2* vs, UDPpacket* pk)
{
	std::ofstream bkfile;
	extendedPacket exPac;
	VESSEL* v = entry->vessel;
	ANIMATION* anim;
	char debugMsg[LINESIZE];

	if (STC_restoring) {
		// the global flag STC_restoring prevents from running this backup
		sprintf(debugMsg, "STC_bkFullState -> return (restoring in process!)");
		tlogging(1, debugMsg);
		return false;
	}
	if (entry == NULL) {
		// it should not happen. Nevertheless, non-registered vessels are not backed up
		sprintf(debugMsg, "STC_bkFullState -> return (no 'entry': non-registered vessel)");
		tlogging(1, debugMsg);
		return false;
	}
	// admin data: warning, can bug if vessel's name includes blank(s)
	char fileName[100], vName[100], vClass[100], vOwner[100];
	strcpy(vOwner, "Boxx"); exPac.ownerNameLength = strlen(vOwner); // provision
	strcpy(vName, v->GetName()); exPac.vesselNameLength = strlen(vName); // yet, callsign=vessel name
	strcpy(vClass, v->GetClassNameA()); exPac.vClassNameLength = strlen(vClass);
	sprintf(fileName, "%s%s%s.data", BKP_PATH.c_str(), BKPREFIX, v->GetName());

	// vessel's default parameters: supplements UDPpacket structure with more data
	exPac.pac = (*pk);
	exPac.pac.content.ID = (entry->GlobalID == NULL) ? 0 : entry->GlobalID->ID; // here we only use Global ID
	//exPac.status: vs->status is NOT reliable => instead, reuse below planet surface collision check (in UDP thread)
	double localSurf, longitude, latitude, radius;
	oapiLocalToEqu(vs->rbody, vs->rpos, &longitude, &latitude, &radius);
	localSurf = oapiGetSize(vs->rbody) + oapiSurfaceElevation(vs->rbody, longitude, latitude); // celestial body not spherical
	if (radius < FLT_MIN) radius = FLT_MIN;
	double height = 0;
	for (int ii = v->GetTouchdownPointCount() - 1; ii >=0; ii--) {
		TOUCHDOWNVTX tdvtx;
		v->GetTouchdownPoint(tdvtx, ii);
		if (height < -tdvtx.pos.y) height = -tdvtx.pos.y; // keeps the lowest tdvtx (in vessel frame)
	}
	if (radius - height < localSurf + 0.1)
	{	// we're likely on the surface (rvel,arot not checked)
		exPac.status = 1; // landed
		exPac.pac.content.arot = _V(10, 0, 0); // <== Undocumented feature "magic value" to land on touchdown points !!IMPORTANT !!It has to be 10, no more, no less !
	} else 		exPac.status = 0; // freeflight
	exPac.surf_lng = vs->surf_lng;
	exPac.surf_lat = vs->surf_lat;
	exPac.surf_hdg = vs->surf_hdg;
	exPac.nfuel = v->GetPropellantCount();
	exPac.nthruster = v->GetThrusterCount();
	exPac.ndockinfo = v->DockCount();
	exPac.nanims = v->GetAnimPtr(&anim);
	exPac.xpdr = vs->xpdr;

	// Writing the current state in a local file
	bkfile.open(fileName, std::ios::trunc | std::ios::binary);
	if (!bkfile.is_open()) {
		bkfile.close();
		sprintf(debugMsg, "STC_bkFullState -> failed to open file %s", fileName);
		tlogging(0, debugMsg);
		return false;
	}
	
	// writing fixed size exPac:
	bkfile.write((char*)(&exPac), sizeof(exPac));

	// vessel's default parameters: now writing variable length data
	bkfile.write(vOwner, exPac.ownerNameLength * sizeof(char));
	bkfile.write(vName, exPac.vesselNameLength * sizeof(char));
	bkfile.write(vClass, exPac.vClassNameLength * sizeof(char));
	// vessel's default parameters: including tanks, thrusters, docks, anims
	for (int ii = exPac.nfuel - 1; ii >= 0; ii--) {
		double fuel[3];
		PROPELLANT_HANDLE tk = v->GetPropellantHandleByIndex(ii);
		fuel[0] = v->GetPropellantEfficiency(tk);
		fuel[1] = v->GetPropellantMaxMass(tk);
		fuel[2] = v->GetPropellantMass(tk);
		bkfile.write((char*)(fuel), sizeof(fuel)); // from 0x0C8 + nfuel*24B
	}
	for (int ii = exPac.nthruster - 1; ii >= 0; ii--) {
		double thrust[2];
		THRUSTER_HANDLE th = v->GetThrusterHandleByIndex(ii);
		thrust[0] = v->GetThrusterMax0(th);
		thrust[1] = v->GetThrusterLevel(th);
		bkfile.write((char*)(thrust), sizeof(thrust)); // + nthruster*16B
	}
	for (int ii = exPac.ndockinfo - 1; ii >= 0; ii--) {
		struct { VECTOR3 p; VECTOR3 d; VECTOR3 r; } dockprm;
		v->GetDockParams(v->GetDockHandle(ii), dockprm.p, dockprm.d, dockprm.r);
		bkfile.write((char*)(&dockprm), sizeof(dockprm)); // + ndockinfo*72B
	}
	for (UINT ii = 0; ii < exPac.nanims; ii++)
	{
		bkfile.write((char*)&(anim[ii].state), sizeof(double)); // + nanims*8B
	}
	bkfile.close();
	return true;
}


/// <summary>
/// Creates and initizes 1 vessel (at a time) from a local backup file named "0[vName].data" with detailed 
/// state at a given time. If the vessel name is alreadu in use, only tanks, thrusters, docks and anims are 
/// updated (no vessel creation). Simulation time is modified to backup's time.
/// /// </summary>
/// <param name="vName">vessel name = call-sign for the moment</param>
/// <warning:>The simulation time shall be set up before the call as close as possible from [vessel]'s 
/// backup mjd time for better performance.
/// <returns>false if the local file could not be found or if the ref.body given in the backup could not 
/// be found in OMX' model.</returns>
bool STC_rsFullState(char* callSign)
{ // warning: "8..." files are those stored by the server, that added 6 bytes at start (SRV_OFFSET)
	IDload* entryB;
	extendedPacket exPac;
	ANIMATION* anim;
	char debugMsg[LINESIZE], buf[24];
	VESSEL* v;
	VESSELSTATUS2 vss;
	VESSELSTATUS2* vs = &vss; // allocation
	OBJHANDLE vH;
	char fileName[100], vName[100], vClass[100], vOwner[100];
	std::ifstream rsfile;

	sprintf(fileName, "%s%s%s.data", BKP_PATH.c_str(), PREFIX, callSign);
	rsfile.open(fileName, std::ios::in | std::ios::binary);
	if (!rsfile.is_open()) {
		rsfile.close();
		sprintf(debugMsg, "STC_rsFullState -> failed to open file %s", fileName);
		tlogging(0, debugMsg);
		return false;
	}

	// reading exPac
	rsfile.seekg(SRV_OFFSET, std::ios::beg);
	rsfile.read((char*)&exPac, sizeof(extendedPacket));
	// then, reconstructing the vesselstatus
	sprintf(buf, "%i", exPac.pac.content.reference);
	if ((entryB = (IDload*)clientData.GlobalsByID->get(buf)) != NULL)
		vs->rbody = entryB->object;
	else {
		rsfile.close();
		sprintf(debugMsg, "STC_rsFullState -> failed due to unknown ref.body in OMX");
		tlogging(0, debugMsg);
		return false;
	}
	vs->base = 0;
	vs->port = 0;
	vs->status = (int)exPac.status; // reliable! (was computed in bkFullState, with arot if needed)
	vs->rpos = exPac.pac.content.pos;
	vs->rvel = exPac.pac.content.vel;
	vs->vrot = exPac.pac.content.vrot;
	vs->arot = exPac.pac.content.arot;
	vs->surf_lng = exPac.surf_lng;
	vs->surf_lat = exPac.surf_lat;
	vs->surf_hdg = exPac.surf_hdg;
	vs->version = 2;
	vs->flag = 0;

	// reading admin data
	if ((0 < exPac.ownerNameLength) && (exPac.ownerNameLength < 99)) {
		rsfile.read(vOwner, exPac.ownerNameLength); vOwner[exPac.ownerNameLength] = 0x0;
	} else strcpy(vOwner, "unknown");
	if ((0 < exPac.vesselNameLength) && (exPac.vesselNameLength < 99)) {
		rsfile.read(vName, exPac.vesselNameLength); vName[exPac.vesselNameLength] = 0x0;
	} else strcpy(vName, "unknown");
	if ((0 < exPac.vClassNameLength) && (exPac.vClassNameLength < 99)) {
		rsfile.read(vClass, exPac.vClassNameLength); vClass[exPac.vClassNameLength] = 0x0;
	} else strcpy(vClass, "unknown");
	// vessel creation:
	oapiSetSimMJD(exPac.pac.content.MJD, PROP_ORBITAL_FIXEDSTATE || PROP_SORBITAL_FIXEDSTATE);
	selfCreate = false; // then, AddVessel(OBJHANDLE obj) will run after creation
	if ((vH = oapiGetVesselByName(vName)) == NULL) vH = oapiCreateVesselEx(vName, vClass, vs);
	v = oapiGetVesselInterface(vH);
	oapiSetFocusObject(vH);

	// after creation: reconstructing the tanks, the thrust and the dock ports
	for (int ii = exPac.nfuel - 1; ii >= 0; ii--) {
		double fuel[3];
		rsfile.read((char*)fuel, 3 * sizeof(double));
		PROPELLANT_HANDLE tk = v->GetPropellantHandleByIndex(ii);
		v->SetPropellantEfficiency(tk, fuel[0]);
		v->SetPropellantMaxMass(tk, fuel[1]);
		v->SetPropellantMass(tk, fuel[2]);
	}
	for (int ii = exPac.nthruster - 1; ii >= 0; ii--) {
		double thrust[2];
		rsfile.read((char*)thrust, 2 * sizeof(double));
		THRUSTER_HANDLE th = v->GetThrusterHandleByIndex(ii);
		v->SetThrusterMax0(th, thrust[0]);
		v->SetThrusterLevel(th, thrust[1]);
	}
	for (int ii = exPac.ndockinfo - 1; ii >= 0; ii--) {
		struct { VECTOR3 p; VECTOR3 d; VECTOR3 r; } dockprm;
		rsfile.read((char*)&dockprm, 3 * sizeof(VECTOR3));
		v->SetDockParams(v->GetDockHandle(ii), dockprm.p, dockprm.d, dockprm.r);
	}
	// reconstructing the animation states
	v->GetAnimPtr(&anim);
	for (UINT ii = 0; ii < exPac.nanims; ii++)
	{
		rsfile.read((char*)&(anim[ii].state), sizeof(double));
	}
	//
	rsfile.close();

	sprintf(debugMsg, "STC_rsFullState (prev.mjd=%.6f) -> %s's %s (%s,%s) was restored (vH%p) @ mjd=%.6f (dt=%.2f[s])", 
		dinfo.snapshotMJD, vOwner, callSign, vName, vClass, vH,
		exPac.pac.content.MJD, 86400.*(exPac.pac.content.MJD-dinfo.snapshotMJD));
	tlogging(1, debugMsg);
	return true;
}

#include <fstream>
#include <filesystem>
#include <vector>
#include <regex>
#include <cstdio>
namespace fs = std::filesystem;
/// <summary>
/// Starts a process where the oldest backup of a vessel's status is restored first. Then, the 
/// simulation time is accelerated to reach the next oldest backup, and so on. Once all files are 
/// restored in the correct sequence,...
/// </summary>
int STC_restoreFromFolder()
{
	char line[LINESIZE], callSign[100];
	double mjd = 0, earliest = -1;
	std::streamoff mjdOff = MJD_OFFSET + SRV_OFFSET;
	std::ifstream bkfile;
	std::string strPattern(CALLSIGNPATTERN);
	std::regex regexPattern(PREFIX + strPattern + "\\.data");

	// let's work with both char* and std::string (!!)
	int pathlength = BKP_PATH.length();
	unsigned __int8 callSignLength;
	char oldFname[256];
	char newFname[256];
	char buffer[BUFFER_SIZE];

	if (STC_progress == next) {
		sprintf(line, "STC_restoreFromFolder -> vName=%s being restored now", STC_nextFileRestore.c_str());
		// at "next" passes, STC_nextFileRestore and STC_nextMJDrestore are already set up:
		bkfile.open((BKP_PATH + STC_nextFileRestore), std::ios::in | std::ios::binary);
		if (!bkfile.is_open()) {
			bkfile.close();
			sprintf(line, "%s, but file was not found!", line); tlogging(0, line);
			STC_progress = none;
			return 0;
		}
		bkfile.seekg(0, std::ios::beg);
		bkfile.read((char*)&buffer, BUFFER_SIZE * sizeof(char));
		bkfile.close();
		callSignLength = (unsigned __int8)buffer[vnameLengthP + SRV_OFFSET];
		strncpy(callSign, &buffer[ownerNameP + SRV_OFFSET] + (unsigned __int8)buffer[ownerLengthP + SRV_OFFSET],
			callSignLength); // does not terminate with \0
		callSign[callSignLength] = 0x0; // terminates the string
		// we create a restored vessel from file and set up the next earliest backup
		strcpy(oldFname, (BKP_PATH + STC_nextFileRestore).c_str());
		strcpy(newFname, oldFname);
		if (STC_nextFileRestore.length() > 0) newFname[BKP_PATH.length()] = '-'; // new prefix
		tlogging(5, line);
		STC_rsFullState(callSign);
		if ((remove(newFname)) != 0) {} //std::cerr << line << "cleaning error " << errno << ", " << std::flush;
		if (rename(oldFname, newFname) != 0) { // EACCES
			sprintf(line, "%s, but renaming failed! Error = %d", line, errno); tlogging(0, line);
		}
		else {
			sprintf(line, "%s => restored", line); tlogging(1, line);
		}
	}

	// searching for earliest backup file time
	for (const auto& fname : fs::directory_iterator(BKP_PATH)) {
		if (fname.is_regular_file()) {
			std::string filename = fname.path().filename().string();
			if (std::regex_match(filename, regexPattern)) {
				bkfile.open((BKP_PATH + filename), std::ios::in | std::ios::binary);
				if (!bkfile.is_open()) {
					sprintf(line, "STC_restoreFromFolder -> failed to open file %s", filename.c_str());
					tlogging(0, line);
				} else {
					bkfile.seekg(mjdOff, std::ios::beg);
					bkfile.read((char*)&mjd, sizeof(double));
					if (earliest < 0 || mjd < earliest) {
						earliest = mjd;
						STC_nextFileRestore = filename;
					}
				}
				bkfile.close();
			}
		}
	}

	sprintf(line, "STC_restoreFromFolder -> %s @ mjd=%.6f", STC_nextFileRestore.c_str(), mjd);
	tlogging(1, line);
	STC_nextMJDrestore = earliest;
	if (earliest < 0) {
		STC_progress = none;
		return 0;
	}

	if (STC_progress == init) {
		// at 1st pass, we re-init the simulation time, set up the first STC_nextFileRestore and STC_nextMJDrestore
		sprintf(line, "STC_restoreFromFolder -> init => oapiSetSimMJD(mjd=%.6f)", STC_nextMJDrestore);
		tlogging(1, line);
		oapiSetSimMJD(STC_nextMJDrestore, PROP_ORBITAL_FIXEDSTATE || PROP_SORBITAL_FIXEDSTATE);
		STC_progress = next;
	} else { //  (STC_progress == next)
		sprintf(line, "STC_restoreFromFolder -> next will occur at mjd=%.6f with fname=%s", STC_nextMJDrestore, STC_nextFileRestore.c_str());
		tlogging(1, line);
	}
	return 1;
}

void STC_keepBackupAlive()
{	// Querying for the process status is unstable. Instead:
	// Checks if ALL "1[callsign].data" are older than a watchdog, meaning that OMX_backupClient has stopped
	char line[LINESIZE];
	char mybuffer[MAX_PATH];
	const std::string exe = "OMX_backupClient.exe";
	const std::string exePath = ".\\";
	std::string commandLine;
	const std::string strPattern(CALLSIGNPATTERN);
	const std::regex regexPattern1("1" + strPattern + "\\.data");
	std::ifstream bkfile;
	HANDLE hProcess;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD result; // , exitCode;
	std::filesystem::file_time_type latest; bool initLatest = false;
	std::filesystem::file_time_type written;
	const std::chrono::seconds watchdog{ 5 }; // [s], maximum delay since a backup file (anyone) was sent

	if (STC_restoring) return; // this is not run while restoring
	if (std::filesystem::_File_time_clock::now() < OMX_backupCheck) return; // too early to check

	// searching for all local backup files "0[callsign].data"
	for (const auto& fname : fs::directory_iterator(BKP_PATH)) {
		if (fname.is_regular_file()) {
			std::string filename = fname.path().filename().string();
			if (std::regex_match(filename, regexPattern1)) {
				written = fname.last_write_time();
				if (initLatest) {if (written > latest) latest = written;}
				else {
					latest = written;
					initLatest = true;
				}
			}
		}
	}

	OMX_backupCheck = std::filesystem::_File_time_clock::now() + oneSec; // next check in 1 sec.
	if (!initLatest) STC_backupPID = NULL; // no "1[callsign].data" file present yet -> (re)run OMX_backupClient
	else if (latest > OMX_backupCheck - watchdog) return; // a backup was sent recently -> no need to (re)run OMX_backupClient


	// if (STC_backupPID && !STC_restoring) {
	// 	// monitors that OMX_backupClient.exe is still running
	// 	// (not during a restore session)
	// 	hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, STC_backupPID);
	// 	if (hProcess != NULL) {
	// 		//if (GetExitCodeProcess(hProcess, &exitCode) == 0)
	// 		//{// If the function fails, the return value is zero. To get extended error information, call GetLastError.
	// 		//	sprintf(line, "(hostsendthread) OMX_backupClient can't give its status, with OS error %d.", GetLastError());
	// 		//	tlogging(0, line);
	// 		//	sprintf(line, "(hostsendthread)                  and was terminated %s",
	// 		//		TerminateProcess(hProcess, 0)? "successfully.":"with error!");
	// 		//	tlogging(0, line);
	// 		//	STC_backupPID = NULL;
	// 		//}
	// 		//else
	// 		if (exitCode != STILL_ACTIVE) STC_backupPID = NULL;
	// 		CloseHandle(hProcess);  // Don't forget to close the handle
	// 	}
	// }

	// launching the independant process OMX_backupClient.exe
	if (STC_backupPID) {
		// NO backup was sent recently, despite OMX_backupClient was launched some time ago
		// => terminates the process if needed
		// => returns to wait for another call to re-launch the process (avoiding multiple calls)
		hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, STC_backupPID);
		if (hProcess != NULL) {
			sprintf(line, "(hostsendthread) OMX_backupClient PID %d was terminated (%s) due to inactivity.",
				STC_backupPID, TerminateProcess(hProcess, 0) ? "successfully" : "with error");
			tlogging(0, line);
			CloseHandle(hProcess); // Don't forget to close the handle
		}
		else {
			sprintf(line, "(hostsendthread) OMX_backupClient PID %d terminated due to unknown reason.", STC_backupPID);
			tlogging(1, line);
		}
		STC_backupPID = NULL;
	} else {
		// if no PID in the system, relaunches OMX_backupClient.exe
		// (not during a restore session)
		commandLine = EXE_PATH + exe + " " + dinfo.srvipchar + " " + itoa(1 + dinfo.srvtcp, line, 10);
		memset(&si, 0, sizeof(si));
		si.cb = sizeof(si);
		memset(&pi, 0, sizeof(pi));
		result = GetCurrentDirectoryA(MAX_PATH, mybuffer);
		if (!CreateProcessA(
			NULL,                        // No module name (use command line)
			&commandLine[0],             // Command line (must be modifiable)
			NULL,                        // Process handle not inheritable
			NULL,                        // Thread handle not inheritable
			FALSE,                       // Set handle inheritance to FALSE
			// CREATE_NEW_CONSOLE, // Detached and new console
			DETACHED_PROCESS | CREATE_NO_WINDOW, // Run in background (no window)
			NULL,                        // Use parent's environment block
			exePath.c_str(),       // Use this directory as starting directory
			&si,                         // Pointer to STARTUPINFO structure
			&pi                          // Pointer to PROCESS_INFORMATION structure
		)) {
			sprintf(line, "(hostsendthread) Can't launch OMX_backupClient, error code = %i", GetLastError());
			logging(0, line);
			STC_backupPID = NULL; // will retry at next loop
		}
		else {
			STC_backupPID = pi.dwProcessId;
			sprintf(line, "(hostsendthread) OMX_backupClient launched with PID %d.", STC_backupPID);
			logging(1, line);
		}
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
}