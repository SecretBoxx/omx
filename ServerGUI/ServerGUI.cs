using Orbiter.Multiplayer;
using System;
using System.Windows.Forms;

namespace Orbiter
{
    class ServerGUI
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Server server = new Server(args);
            server.Start();
            Application.Run();
        }
    }
}
