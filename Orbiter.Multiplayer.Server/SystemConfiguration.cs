﻿//Name             : Orbiter System Configuration
//Version          : 1.0
//Author           : Kastner-Masilko Friedrich
//About            : This grammar is used to parse Orbiter system configuration files.
//Case Sensitive   : 
//Start Symbol     : 

using GoldParser;

namespace Orbiter.Multiplayer
{
	public enum SystemConfigurationSymbols : int
	{
		/// <summary> (EOF) </summary>
		S_EOF         = 0,     
		/// <summary> (Error) </summary>
		S_Error       = 1,     
		/// <summary> Comment </summary>
		S_Comment     = 2,     
		/// <summary> ';' </summary>
		S_Semi        = 3,     
		/// <summary> ':' </summary>
		S_Colon       = 4,     
		/// <summary> '=' </summary>
		S_Eq          = 5,     
		/// <summary> Counter </summary>
		S_Counter     = 6,     
		/// <summary> Identifier </summary>
		S_Identifier  = 7,     
		/// <summary> MarkerPath </summary>
		S_MarkerPath  = 8,     
		/// <summary> Moon </summary>
		S_Moon        = 9,     
		/// <summary> Name </summary>
		S_Name        = 10,     
		/// <summary> NewLine </summary>
		S_NewLine     = 11,     
		/// <summary> Planet </summary>
		S_Planet      = 12,     
		/// <summary> Star </summary>
		S_Star        = 13,     
		/// <summary> WS </summary>
		S_WS          = 14,     
		/// <summary> &lt;Break&gt; </summary>
		S_Break       = 15,     
		/// <summary> &lt;Document&gt; </summary>
		S_Document    = 16,     
		/// <summary> &lt;Id&gt; </summary>
		S_Id          = 17,     
		/// <summary> &lt;Line&gt; </summary>
		S_Line        = 18,     
		/// <summary> &lt;Lines&gt; </summary>
		S_Lines       = 19,     
		/// <summary> &lt;MarkerPath&gt; </summary>
		S_MarkerPath2 = 20,     
		/// <summary> &lt;Moon&gt; </summary>
		S_Moon2       = 21,     
		/// <summary> &lt;Name&gt; </summary>
		S_Name2       = 22,     
		/// <summary> &lt;Planet&gt; </summary>
		S_Planet2     = 23,     
		/// <summary> &lt;Star&gt; </summary>
		S_Star2       = 24,     
		/// <summary> &lt;WL&gt; </summary>
		S_WL          = 25,     
		/// <summary> &lt;WS&gt; </summary>
		S_WS2         = 26      
	}
	
	public enum SystemConfigurationRules : int
	{
		/// <summary> &lt;WS&gt; ::= WS </summary>
		WS_WS                      = 0,    
		/// <summary> &lt;WS&gt; ::=  </summary>
		WS                         = 1,    
		/// <summary> &lt;Break&gt; ::= &lt;WS&gt; NewLine &lt;WL&gt; </summary>
		Break_NewLine              = 2,    
		/// <summary> &lt;Break&gt; ::= &lt;WS&gt; NewLine </summary>
		Break_NewLine2             = 3,    
		/// <summary> &lt;WL&gt; ::= &lt;Break&gt; </summary>
		WL                         = 4,    
		/// <summary> &lt;WL&gt; ::= WS </summary>
		WL_WS                      = 5,    
		/// <summary> &lt;Document&gt; ::= &lt;WL&gt; &lt;Name&gt; &lt;Break&gt; &lt;MarkerPath&gt; &lt;Break&gt; &lt;Lines&gt; &lt;WL&gt; </summary>
		Document                   = 6,    
		/// <summary> &lt;Document&gt; ::= &lt;Name&gt; &lt;Break&gt; &lt;MarkerPath&gt; &lt;Break&gt; &lt;Lines&gt; &lt;WL&gt; </summary>
		Document2                  = 7,    
		/// <summary> &lt;Document&gt; ::= &lt;WL&gt; &lt;Name&gt; &lt;Break&gt; &lt;MarkerPath&gt; &lt;Break&gt; &lt;Lines&gt; </summary>
		Document3                  = 8,    
		/// <summary> &lt;Document&gt; ::= &lt;Name&gt; &lt;Break&gt; &lt;MarkerPath&gt; &lt;Break&gt; &lt;Lines&gt; </summary>
		Document4                  = 9,    
		/// <summary> &lt;Document&gt; ::= &lt;WL&gt; &lt;Name&gt; &lt;Break&gt; &lt;Lines&gt; &lt;WL&gt; </summary>
		Document5                  = 10,    
		/// <summary> &lt;Document&gt; ::= &lt;Name&gt; &lt;Break&gt; &lt;Lines&gt; &lt;WL&gt; </summary>
		Document6                  = 11,    
		/// <summary> &lt;Document&gt; ::= &lt;WL&gt; &lt;Name&gt; &lt;Break&gt; &lt;Lines&gt; </summary>
		Document7                  = 12,    
		/// <summary> &lt;Document&gt; ::= &lt;Name&gt; &lt;Break&gt; &lt;Lines&gt; </summary>
		Document8                  = 13,    
		/// <summary> &lt;Lines&gt; ::= &lt;Lines&gt; &lt;Break&gt; &lt;Line&gt; </summary>
		Lines                      = 14,    
		/// <summary> &lt;Lines&gt; ::= &lt;Line&gt; </summary>
		Lines2                     = 15,    
		/// <summary> &lt;Line&gt; ::= &lt;Star&gt; </summary>
		Line                       = 16,    
		/// <summary> &lt;Line&gt; ::= &lt;Planet&gt; </summary>
		Line2                      = 17,    
		/// <summary> &lt;Line&gt; ::= &lt;Moon&gt; </summary>
		Line3                      = 18,    
		/// <summary> &lt;Name&gt; ::= Name &lt;WS&gt; '=' &lt;WS&gt; &lt;Id&gt; </summary>
		Name_Name_Eq               = 19,    
		/// <summary> &lt;MarkerPath&gt; ::= MarkerPath &lt;WS&gt; '=' &lt;WS&gt; &lt;Id&gt; </summary>
		MarkerPath_MarkerPath_Eq   = 20,    
		/// <summary> &lt;Star&gt; ::= Star Counter &lt;WS&gt; '=' &lt;WS&gt; &lt;Id&gt; </summary>
		Star_Star_Counter_Eq       = 21,    
		/// <summary> &lt;Planet&gt; ::= Planet Counter &lt;WS&gt; '=' &lt;WS&gt; &lt;Id&gt; </summary>
		Planet_Planet_Counter_Eq   = 22,    
		/// <summary> &lt;Moon&gt; ::= &lt;Id&gt; &lt;WS&gt; ':' &lt;WS&gt; Moon Counter &lt;WS&gt; '=' &lt;WS&gt; &lt;Id&gt; </summary>
		Moon_Colon_Moon_Counter_Eq = 23,    
		/// <summary> &lt;Id&gt; ::= Counter </summary>
		Id_Counter                 = 24,    
		/// <summary> &lt;Id&gt; ::= Identifier </summary>
		Id_Identifier              = 25,    
		/// <summary> &lt;Id&gt; ::= Moon </summary>
		Id_Moon                    = 26,    
		/// <summary> &lt;Id&gt; ::= Planet </summary>
		Id_Planet                  = 27,    
		/// <summary> &lt;Id&gt; ::= Star </summary>
		Id_Star                    = 28,    
		/// <summary> &lt;Id&gt; ::= MarkerPath </summary>
		Id_MarkerPath              = 29,    
		/// <summary> &lt;Id&gt; ::= Name </summary>
		Id_Name                    = 30     
	}
	
	public class SystemConfigurationNode : SyntaxNode
	{
		public SystemConfigurationNode(Rule rule, SyntaxNode[] nodes) : base(rule, nodes) {}
		
		public SystemConfigurationNode(Symbol symbol, string content) : base(symbol, content) {}
		
		public new SystemConfigurationSymbols SymbolIndex
		{
			get
			{
				return (SystemConfigurationSymbols) base.SymbolIndex;
			}
		}
		
		public new SystemConfigurationRules RuleIndex
		{
			get
			{
				return (SystemConfigurationRules) base.RuleIndex;
			}
		}
		
		public new SystemConfigurationNode this[int index]
		{
			get
			{
				return (SystemConfigurationNode) base[index];
			}
		}
		
		public new SystemConfigurationNode Trimmed
		{
			get
			{
				return (SystemConfigurationNode) base.Trimmed;
			}
		}
	}
}
