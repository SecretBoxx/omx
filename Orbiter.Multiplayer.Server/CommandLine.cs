﻿//Name             : OMP-server commands
//Version          : 1.1
//Author           : Kastner-Masilko Friedrich
//About            : This grammar is used to parse OMP-server commands sent by OMP-clients. Command added from v1.1
//Case Sensitive   : 
//Start Symbol     : 

using GoldParser;

namespace Orbiter.Multiplayer
{
	public enum CommandLineSymbols : int
	{
		/// <summary> (EOF) </summary>
		S_EOF             = 0,     
		/// <summary> (Error) </summary>
		S_Error           = 1,     
		/// <summary> Whitespace </summary>
		S_Whitespace      = 2,     
		/// <summary> a </summary>
		S_a               = 3,     
		/// <summary> accept </summary>
		S_accept          = 4,     
		/// <summary> all </summary>
		S_all             = 5,     
		/// <summary> as </summary>
		S_as              = 6,     
		/// <summary> assign </summary>
		S_assign          = 7,     
		/// <summary> c </summary>
		S_c               = 8,     
		/// <summary> check </summary>
		S_check           = 9,     
		/// <summary> chk </summary>
		S_chk             = 10,     
		/// <summary> cl </summary>
		S_cl              = 11,     
		/// <summary> clients </summary>
		S_clients         = 12,     
		/// <summary> cn </summary>
		S_cn              = 13,     
		/// <summary> connections </summary>
		S_connections     = 14,     
		/// <summary> current </summary>
		S_current         = 15,     
		/// <summary> cut </summary>
		S_cut             = 16,     
		/// <summary> d </summary>
		S_d               = 17,     
		/// <summary> data </summary>
		S_data            = 18,     
		/// <summary> detailed </summary>
		S_detailed        = 19,     
		/// <summary> dump </summary>
		S_dump            = 20,     
		/// <summary> echo </summary>
		S_echo            = 21,     
		/// <summary> EscapedConst </summary>
		S_EscapedConst    = 22,     
		/// <summary> exit </summary>
		S_exit            = 23,     
		/// <summary> f </summary>
		S_f               = 24,     
		/// <summary> files </summary>
		S_files           = 25,     
		/// <summary> from </summary>
		S_from            = 26,     
		/// <summary> g </summary>
		S_g               = 27,     
		/// <summary> gbodies </summary>
		S_gbodies         = 28,     
		/// <summary> GINFO </summary>
		S_GINFO           = 29,     
		/// <summary> gl </summary>
		S_gl              = 30,     
		/// <summary> globals </summary>
		S_globals         = 31,     
		/// <summary> h </summary>
		S_h               = 32,     
		/// <summary> help </summary>
		S_help            = 33,     
		/// <summary> ip </summary>
		S_ip              = 34,     
		/// <summary> jn </summary>
		S_jn              = 35,     
		/// <summary> join </summary>
		S_join            = 36,     
		/// <summary> kick </summary>
		S_kick            = 37,     
		/// <summary> kickban </summary>
		S_kickban         = 38,     
		/// <summary> l </summary>
		S_l               = 39,     
		/// <summary> last </summary>
		S_last            = 40,     
		/// <summary> leave </summary>
		S_leave           = 41,     
		/// <summary> list </summary>
		S_list            = 42,     
		/// <summary> location </summary>
		S_location        = 43,     
		/// <summary> log </summary>
		S_log             = 44,     
		/// <summary> ls </summary>
		S_ls              = 45,     
		/// <summary> lv </summary>
		S_lv              = 46,     
		/// <summary> me </summary>
		S_me              = 47,     
		/// <summary> mjd </summary>
		S_mjd             = 48,     
		/// <summary> nick </summary>
		S_nick            = 49,     
		/// <summary> nk </summary>
		S_nk              = 50,     
		/// <summary> NormalConst </summary>
		S_NormalConst     = 51,     
		/// <summary> o </summary>
		S_o               = 52,     
		/// <summary> ob </summary>
		S_ob              = 53,     
		/// <summary> objects </summary>
		S_objects         = 54,     
		/// <summary> obs </summary>
		S_obs             = 55,     
		/// <summary> observe </summary>
		S_observe         = 56,     
		/// <summary> off </summary>
		S_off             = 57,     
		/// <summary> offer </summary>
		S_offer           = 58,     
		/// <summary> on </summary>
		S_on              = 59,     
		/// <summary> own </summary>
		S_own             = 60,     
		/// <summary> ping </summary>
		S_ping            = 61,     
		/// <summary> pr </summary>
		S_pr              = 62,     
		/// <summary> prompt </summary>
		S_prompt          = 63,     
		/// <summary> quit </summary>
		S_quit            = 64,     
		/// <summary> r </summary>
		S_r               = 65,     
		/// <summary> RADIO </summary>
		S_RADIO           = 66,     
		/// <summary> REQST </summary>
		S_REQST           = 67,     
		/// <summary> reset </summary>
		S_reset           = 68,     
		/// <summary> s </summary>
		S_s               = 69,     
		/// <summary> st </summary>
		S_st              = 70,     
		/// <summary> status </summary>
		S_status          = 71,     
		/// <summary> StringConst </summary>
		S_StringConst     = 72,     
		/// <summary> sync </summary>
		S_sync            = 73,     
		/// <summary> system </summary>
		S_system          = 74,     
		/// <summary> t </summary>
		S_t               = 75,     
		/// <summary> talk </summary>
		S_talk            = 76,     
		/// <summary> time </summary>
		S_time            = 77,     
		/// <summary> tkbck </summary>
		S_tkbck           = 78,     
		/// <summary> track </summary>
		S_track           = 79,     
		/// <summary> trk </summary>
		S_trk             = 80,     
		/// <summary> u </summary>
		S_u               = 81,     
		/// <summary> users </summary>
		S_users           = 82,     
		/// <summary> v </summary>
		S_v               = 83,     
		/// <summary> version </summary>
		S_version         = 84,     
		/// <summary> vessels </summary>
		S_vessels         = 85,     
		/// <summary> vs </summary>
		S_vs              = 86,     
		/// <summary> &lt;AdminConst&gt; </summary>
		S_AdminConst      = 87,     
		/// <summary> &lt;Check&gt; </summary>
		S_Check2          = 88,     
		/// <summary> &lt;CheckConst&gt; </summary>
		S_CheckConst      = 89,     
		/// <summary> &lt;CheckValue&gt; </summary>
		S_CheckValue      = 90,     
		/// <summary> &lt;Command&gt; </summary>
		S_Command         = 91,     
		/// <summary> &lt;CommandConst&gt; </summary>
		S_CommandConst    = 92,     
		/// <summary> &lt;Dump&gt; </summary>
		S_Dump2           = 93,     
		/// <summary> &lt;Echo&gt; </summary>
		S_Echo2           = 94,     
		/// <summary> &lt;List&gt; </summary>
		S_List2           = 95,     
		/// <summary> &lt;ListConst&gt; </summary>
		S_ListConst       = 96,     
		/// <summary> &lt;ListDetailConst&gt; </summary>
		S_ListDetailConst = 97,     
		/// <summary> &lt;ListListConst&gt; </summary>
		S_ListListConst   = 98,     
		/// <summary> &lt;ListSelectValue&gt; </summary>
		S_ListSelectValue = 99,     
		/// <summary> &lt;ListTargetConst&gt; </summary>
		S_ListTargetConst = 100,     
		/// <summary> &lt;Log&gt; </summary>
		S_Log2            = 101,     
		/// <summary> &lt;LogValue&gt; </summary>
		S_LogValue        = 102,     
		/// <summary> &lt;MJD&gt; </summary>
		S_MJD2            = 103,     
		/// <summary> &lt;Nick&gt; </summary>
		S_Nick2           = 104,     
		/// <summary> &lt;StandardValue&gt; </summary>
		S_StandardValue   = 105,     
		/// <summary> &lt;Sync&gt; </summary>
		S_Sync2           = 106,     
		/// <summary> &lt;Timing&gt; </summary>
		S_Timing          = 107,     
		/// <summary> &lt;Track&gt; </summary>
		S_Track2          = 108,     
		/// <summary> &lt;Value&gt; </summary>
		S_Value           = 109      
	}
	
	public enum CommandLineRules : int
	{
		/// <summary> &lt;Command&gt; ::= as &lt;Value&gt; &lt;Value&gt; &lt;Nick&gt; </summary>
		Command_as                 = 0,    
		/// <summary> &lt;Command&gt; ::= assign &lt;Value&gt; &lt;Value&gt; &lt;Nick&gt; </summary>
		Command_assign             = 1,    
		/// <summary> &lt;Command&gt; ::= accept &lt;Value&gt; &lt;Value&gt; </summary>
		Command_accept             = 2,    
		/// <summary> &lt;Command&gt; ::= accept </summary>
		Command_accept2            = 3,    
		/// <summary> &lt;Command&gt; ::= chk &lt;Check&gt; </summary>
		Command_chk                = 4,    
		/// <summary> &lt;Command&gt; ::= check &lt;Check&gt; </summary>
		Command_check              = 5,    
		/// <summary> &lt;Command&gt; ::= echo &lt;Echo&gt; </summary>
		Command_echo               = 6,    
		/// <summary> &lt;Command&gt; ::= GINFO &lt;Value&gt; &lt;Value&gt; &lt;Value&gt; </summary>
		Command_GINFO              = 7,    
		/// <summary> &lt;Command&gt; ::= GINFO &lt;Value&gt; &lt;Value&gt; </summary>
		Command_GINFO2             = 8,    
		/// <summary> &lt;Command&gt; ::= ip &lt;Value&gt; </summary>
		Command_ip                 = 9,    
		/// <summary> &lt;Command&gt; ::= ip </summary>
		Command_ip2                = 10,    
		/// <summary> &lt;Command&gt; ::= jn &lt;Value&gt; &lt;Value&gt; &lt;Value&gt; </summary>
		Command_jn                 = 11,    
		/// <summary> &lt;Command&gt; ::= join &lt;Value&gt; &lt;Value&gt; &lt;Value&gt; </summary>
		Command_join               = 12,    
		/// <summary> &lt;Command&gt; ::= lv &lt;Value&gt; &lt;Value&gt; </summary>
		Command_lv                 = 13,    
		/// <summary> &lt;Command&gt; ::= leave &lt;Value&gt; &lt;Value&gt; </summary>
		Command_leave              = 14,    
		/// <summary> &lt;Command&gt; ::= ls &lt;List&gt; </summary>
		Command_ls                 = 15,    
		/// <summary> &lt;Command&gt; ::= list &lt;List&gt; </summary>
		Command_list               = 16,    
		/// <summary> &lt;Command&gt; ::= quit </summary>
		Command_quit               = 17,    
		/// <summary> &lt;Command&gt; ::= exit </summary>
		Command_exit               = 18,    
		/// <summary> &lt;Command&gt; ::= RADIO EscapedConst </summary>
		Command_RADIO_EscapedConst = 19,    
		/// <summary> &lt;Command&gt; ::= REQST &lt;Value&gt; </summary>
		Command_REQST              = 20,    
		/// <summary> &lt;Command&gt; ::= REQST ping &lt;Value&gt; </summary>
		Command_REQST_ping         = 21,    
		/// <summary> &lt;Command&gt; ::= REQST ping &lt;Value&gt; from &lt;Value&gt; </summary>
		Command_REQST_ping_from    = 22,    
		/// <summary> &lt;Command&gt; ::= t EscapedConst </summary>
		Command_t_EscapedConst     = 23,    
		/// <summary> &lt;Command&gt; ::= talk EscapedConst </summary>
		Command_talk_EscapedConst  = 24,    
		/// <summary> &lt;Command&gt; ::= tkbck &lt;Value&gt; </summary>
		Command_tkbck              = 25,    
		/// <summary> &lt;Command&gt; ::= me EscapedConst </summary>
		Command_me_EscapedConst    = 26,    
		/// <summary> &lt;Command&gt; ::= trk &lt;Track&gt; </summary>
		Command_trk                = 27,    
		/// <summary> &lt;Command&gt; ::= track &lt;Track&gt; </summary>
		Command_track              = 28,    
		/// <summary> &lt;Command&gt; ::= d &lt;Dump&gt; </summary>
		Command_d                  = 29,    
		/// <summary> &lt;Command&gt; ::= dump &lt;Dump&gt; </summary>
		Command_dump               = 30,    
		/// <summary> &lt;Command&gt; ::= log &lt;Log&gt; </summary>
		Command_log                = 31,    
		/// <summary> &lt;Command&gt; ::= mjd &lt;MJD&gt; </summary>
		Command_mjd                = 32,    
		/// <summary> &lt;Command&gt; ::= sync &lt;Sync&gt; </summary>
		Command_sync               = 33,    
		/// <summary> &lt;Command&gt; ::= h </summary>
		Command_h                  = 34,    
		/// <summary> &lt;Command&gt; ::= help </summary>
		Command_help               = 35,    
		/// <summary> &lt;Command&gt; ::= nk &lt;Nick&gt; </summary>
		Command_nk                 = 36,    
		/// <summary> &lt;Command&gt; ::= nick &lt;Nick&gt; </summary>
		Command_nick               = 37,    
		/// <summary> &lt;Command&gt; ::= prompt &lt;Nick&gt; </summary>
		Command_prompt             = 38,    
		/// <summary> &lt;Command&gt; ::= pr &lt;Nick&gt; </summary>
		Command_pr                 = 39,    
		/// <summary> &lt;Command&gt; ::= observe &lt;Track&gt; </summary>
		Command_observe            = 40,    
		/// <summary> &lt;Command&gt; ::= obs &lt;Track&gt; </summary>
		Command_obs                = 41,    
		/// <summary> &lt;Command&gt; ::= offer &lt;Value&gt; &lt;Value&gt; </summary>
		Command_offer              = 42,    
		/// <summary> &lt;Command&gt; ::= offer off &lt;StandardValue&gt; </summary>
		Command_offer_off          = 43,    
		/// <summary> &lt;Command&gt; ::= offer off </summary>
		Command_offer_off2         = 44,    
		/// <summary> &lt;Command&gt; ::= offer </summary>
		Command_offer2             = 45,    
		/// <summary> &lt;Command&gt; ::= kick &lt;Value&gt; </summary>
		Command_kick               = 46,    
		/// <summary> &lt;Command&gt; ::= kickban &lt;Track&gt; </summary>
		Command_kickban            = 47,    
		/// <summary> &lt;Check&gt; ::= f </summary>
		Check_f                    = 48,    
		/// <summary> &lt;Check&gt; ::= files </summary>
		Check_files                = 49,    
		/// <summary> &lt;Check&gt; ::= s </summary>
		Check_s                    = 50,    
		/// <summary> &lt;Check&gt; ::= system </summary>
		Check_system               = 51,    
		/// <summary> &lt;Check&gt; ::= t </summary>
		Check_t                    = 52,    
		/// <summary> &lt;Check&gt; ::= time </summary>
		Check_time                 = 53,    
		/// <summary> &lt;Check&gt; ::= v </summary>
		Check_v                    = 54,    
		/// <summary> &lt;Check&gt; ::= version </summary>
		Check_version              = 55,    
		/// <summary> &lt;Check&gt; ::= vs </summary>
		Check_vs                   = 56,    
		/// <summary> &lt;Check&gt; ::= vessels </summary>
		Check_vessels              = 57,    
		/// <summary> &lt;Check&gt; ::= l &lt;Nick&gt; </summary>
		Check_l                    = 58,    
		/// <summary> &lt;Check&gt; ::= location &lt;Nick&gt; </summary>
		Check_location             = 59,    
		/// <summary> &lt;Check&gt; ::= g </summary>
		Check_g                    = 60,    
		/// <summary> &lt;Check&gt; ::= gbodies </summary>
		Check_gbodies              = 61,    
		/// <summary> &lt;Check&gt; ::= &lt;CheckValue&gt; </summary>
		Check                      = 62,    
		/// <summary> &lt;Echo&gt; ::= on </summary>
		Echo_on                    = 63,    
		/// <summary> &lt;Echo&gt; ::= off </summary>
		Echo_off                   = 64,    
		/// <summary> &lt;Echo&gt; ::=  </summary>
		Echo                       = 65,    
		/// <summary> &lt;List&gt; ::= &lt;ListListConst&gt; </summary>
		List                       = 66,    
		/// <summary> &lt;List&gt; ::= &lt;ListTargetConst&gt; </summary>
		List2                      = 67,    
		/// <summary> &lt;List&gt; ::= &lt;ListDetailConst&gt; </summary>
		List3                      = 68,    
		/// <summary> &lt;List&gt; ::= &lt;ListSelectValue&gt; </summary>
		List4                      = 69,    
		/// <summary> &lt;List&gt; ::= &lt;ListListConst&gt; &lt;ListTargetConst&gt; </summary>
		List5                      = 70,    
		/// <summary> &lt;List&gt; ::= &lt;ListSelectValue&gt; &lt;ListTargetConst&gt; </summary>
		List6                      = 71,    
		/// <summary> &lt;List&gt; ::= &lt;ListListConst&gt; &lt;ListDetailConst&gt; </summary>
		List7                      = 72,    
		/// <summary> &lt;List&gt; ::= &lt;ListSelectValue&gt; &lt;ListDetailConst&gt; </summary>
		List8                      = 73,    
		/// <summary> &lt;List&gt; ::= &lt;ListTargetConst&gt; &lt;ListDetailConst&gt; </summary>
		List9                      = 74,    
		/// <summary> &lt;List&gt; ::= &lt;ListListConst&gt; &lt;ListTargetConst&gt; &lt;ListDetailConst&gt; </summary>
		List10                     = 75,    
		/// <summary> &lt;List&gt; ::= &lt;ListSelectValue&gt; &lt;ListTargetConst&gt; &lt;ListDetailConst&gt; </summary>
		List11                     = 76,    
		/// <summary> &lt;List&gt; ::=  </summary>
		List12                     = 77,    
		/// <summary> &lt;Track&gt; ::= off </summary>
		Track_off                  = 78,    
		/// <summary> &lt;Track&gt; ::= off &lt;StandardValue&gt; </summary>
		Track_off2                 = 79,    
		/// <summary> &lt;Track&gt; ::= &lt;StandardValue&gt; </summary>
		Track                      = 80,    
		/// <summary> &lt;Track&gt; ::=  </summary>
		Track2                     = 81,    
		/// <summary> &lt;Dump&gt; ::= globals </summary>
		Dump_globals               = 82,    
		/// <summary> &lt;Dump&gt; ::= gl </summary>
		Dump_gl                    = 83,    
		/// <summary> &lt;Dump&gt; ::= connections </summary>
		Dump_connections           = 84,    
		/// <summary> &lt;Dump&gt; ::= cn </summary>
		Dump_cn                    = 85,    
		/// <summary> &lt;Dump&gt; ::= clients </summary>
		Dump_clients               = 86,    
		/// <summary> &lt;Dump&gt; ::= cl </summary>
		Dump_cl                    = 87,    
		/// <summary> &lt;Dump&gt; ::= log &lt;StandardValue&gt; &lt;Timing&gt; </summary>
		Dump_log                   = 88,    
		/// <summary> &lt;Dump&gt; ::= l &lt;StandardValue&gt; &lt;Timing&gt; </summary>
		Dump_l                     = 89,    
		/// <summary> &lt;Dump&gt; ::=  </summary>
		Dump                       = 90,    
		/// <summary> &lt;Timing&gt; ::= last </summary>
		Timing_last                = 91,    
		/// <summary> &lt;Timing&gt; ::= l </summary>
		Timing_l                   = 92,    
		/// <summary> &lt;Timing&gt; ::= &lt;StandardValue&gt; </summary>
		Timing                     = 93,    
		/// <summary> &lt;Timing&gt; ::=  </summary>
		Timing2                    = 94,    
		/// <summary> &lt;Log&gt; ::= cut </summary>
		Log_cut                    = 95,    
		/// <summary> &lt;Log&gt; ::= &lt;LogValue&gt; </summary>
		Log                        = 96,    
		/// <summary> &lt;Log&gt; ::=  </summary>
		Log2                       = 97,    
		/// <summary> &lt;MJD&gt; ::= status </summary>
		MJD_status                 = 98,    
		/// <summary> &lt;MJD&gt; ::= st </summary>
		MJD_st                     = 99,    
		/// <summary> &lt;MJD&gt; ::= reset &lt;Value&gt; &lt;Value&gt; &lt;Value&gt; </summary>
		MJD_reset                  = 100,    
		/// <summary> &lt;MJD&gt; ::= r &lt;Value&gt; &lt;Value&gt; &lt;Value&gt; </summary>
		MJD_r                      = 101,    
		/// <summary> &lt;Sync&gt; ::= status </summary>
		Sync_status                = 102,    
		/// <summary> &lt;Sync&gt; ::= reset </summary>
		Sync_reset                 = 103,    
		/// <summary> &lt;Sync&gt; ::= data </summary>
		Sync_data                  = 104,    
		/// <summary> &lt;Nick&gt; ::= &lt;Value&gt; </summary>
		Nick                       = 105,    
		/// <summary> &lt;Nick&gt; ::=  </summary>
		Nick2                      = 106,    
		/// <summary> &lt;StandardValue&gt; ::= NormalConst </summary>
		StandardValue_NormalConst  = 107,    
		/// <summary> &lt;StandardValue&gt; ::= StringConst </summary>
		StandardValue_StringConst  = 108,    
		/// <summary> &lt;StandardValue&gt; ::= EscapedConst </summary>
		StandardValue_EscapedConst = 109,    
		/// <summary> &lt;CommandConst&gt; ::= as </summary>
		CommandConst_as            = 110,    
		/// <summary> &lt;CommandConst&gt; ::= accept </summary>
		CommandConst_accept        = 111,    
		/// <summary> &lt;CommandConst&gt; ::= assign </summary>
		CommandConst_assign        = 112,    
		/// <summary> &lt;CommandConst&gt; ::= chk </summary>
		CommandConst_chk           = 113,    
		/// <summary> &lt;CommandConst&gt; ::= check </summary>
		CommandConst_check         = 114,    
		/// <summary> &lt;CommandConst&gt; ::= echo </summary>
		CommandConst_echo          = 115,    
		/// <summary> &lt;CommandConst&gt; ::= GINFO </summary>
		CommandConst_GINFO         = 116,    
		/// <summary> &lt;CommandConst&gt; ::= ip </summary>
		CommandConst_ip            = 117,    
		/// <summary> &lt;CommandConst&gt; ::= jn </summary>
		CommandConst_jn            = 118,    
		/// <summary> &lt;CommandConst&gt; ::= join </summary>
		CommandConst_join          = 119,    
		/// <summary> &lt;CommandConst&gt; ::= lv </summary>
		CommandConst_lv            = 120,    
		/// <summary> &lt;CommandConst&gt; ::= leave </summary>
		CommandConst_leave         = 121,    
		/// <summary> &lt;CommandConst&gt; ::= ls </summary>
		CommandConst_ls            = 122,    
		/// <summary> &lt;CommandConst&gt; ::= list </summary>
		CommandConst_list          = 123,    
		/// <summary> &lt;CommandConst&gt; ::= quit </summary>
		CommandConst_quit          = 124,    
		/// <summary> &lt;CommandConst&gt; ::= exit </summary>
		CommandConst_exit          = 125,    
		/// <summary> &lt;CommandConst&gt; ::= RADIO </summary>
		CommandConst_RADIO         = 126,    
		/// <summary> &lt;CommandConst&gt; ::= REQST </summary>
		CommandConst_REQST         = 127,    
		/// <summary> &lt;CommandConst&gt; ::= talk </summary>
		CommandConst_talk          = 128,    
		/// <summary> &lt;CommandConst&gt; ::= me </summary>
		CommandConst_me            = 129,    
		/// <summary> &lt;CommandConst&gt; ::= trk </summary>
		CommandConst_trk           = 130,    
		/// <summary> &lt;CommandConst&gt; ::= track </summary>
		CommandConst_track         = 131,    
		/// <summary> &lt;CommandConst&gt; ::= dump </summary>
		CommandConst_dump          = 132,    
		/// <summary> &lt;CommandConst&gt; ::= log </summary>
		CommandConst_log           = 133,    
		/// <summary> &lt;CommandConst&gt; ::= mjd </summary>
		CommandConst_mjd           = 134,    
		/// <summary> &lt;CommandConst&gt; ::= sync </summary>
		CommandConst_sync          = 135,    
		/// <summary> &lt;CommandConst&gt; ::= h </summary>
		CommandConst_h             = 136,    
		/// <summary> &lt;CommandConst&gt; ::= help </summary>
		CommandConst_help          = 137,    
		/// <summary> &lt;CommandConst&gt; ::= nk </summary>
		CommandConst_nk            = 138,    
		/// <summary> &lt;CommandConst&gt; ::= nick </summary>
		CommandConst_nick          = 139,    
		/// <summary> &lt;CommandConst&gt; ::= ping </summary>
		CommandConst_ping          = 140,    
		/// <summary> &lt;CommandConst&gt; ::= from </summary>
		CommandConst_from          = 141,    
		/// <summary> &lt;CommandConst&gt; ::= prompt </summary>
		CommandConst_prompt        = 142,    
		/// <summary> &lt;CommandConst&gt; ::= pr </summary>
		CommandConst_pr            = 143,    
		/// <summary> &lt;CommandConst&gt; ::= observe </summary>
		CommandConst_observe       = 144,    
		/// <summary> &lt;CommandConst&gt; ::= obs </summary>
		CommandConst_obs           = 145,    
		/// <summary> &lt;CommandConst&gt; ::= offer </summary>
		CommandConst_offer         = 146,    
		/// <summary> &lt;CommandConst&gt; ::= kick </summary>
		CommandConst_kick          = 147,    
		/// <summary> &lt;CommandConst&gt; ::= kickban </summary>
		CommandConst_kickban       = 148,    
		/// <summary> &lt;CheckConst&gt; ::= f </summary>
		CheckConst_f               = 149,    
		/// <summary> &lt;CheckConst&gt; ::= files </summary>
		CheckConst_files           = 150,    
		/// <summary> &lt;CheckConst&gt; ::= s </summary>
		CheckConst_s               = 151,    
		/// <summary> &lt;CheckConst&gt; ::= system </summary>
		CheckConst_system          = 152,    
		/// <summary> &lt;CheckConst&gt; ::= t </summary>
		CheckConst_t               = 153,    
		/// <summary> &lt;CheckConst&gt; ::= time </summary>
		CheckConst_time            = 154,    
		/// <summary> &lt;CheckConst&gt; ::= v </summary>
		CheckConst_v               = 155,    
		/// <summary> &lt;CheckConst&gt; ::= version </summary>
		CheckConst_version         = 156,    
		/// <summary> &lt;CheckConst&gt; ::= vs </summary>
		CheckConst_vs              = 157,    
		/// <summary> &lt;CheckConst&gt; ::= vessels </summary>
		CheckConst_vessels         = 158,    
		/// <summary> &lt;CheckConst&gt; ::= location </summary>
		CheckConst_location        = 159,    
		/// <summary> &lt;CheckConst&gt; ::= g </summary>
		CheckConst_g               = 160,    
		/// <summary> &lt;CheckConst&gt; ::= gbodies </summary>
		CheckConst_gbodies         = 161,    
		/// <summary> &lt;ListListConst&gt; ::= a </summary>
		ListListConst_a            = 162,    
		/// <summary> &lt;ListListConst&gt; ::= all </summary>
		ListListConst_all          = 163,    
		/// <summary> &lt;ListListConst&gt; ::= o </summary>
		ListListConst_o            = 164,    
		/// <summary> &lt;ListListConst&gt; ::= own </summary>
		ListListConst_own          = 165,    
		/// <summary> &lt;ListListConst&gt; ::= c </summary>
		ListListConst_c            = 166,    
		/// <summary> &lt;ListListConst&gt; ::= current </summary>
		ListListConst_current      = 167,    
		/// <summary> &lt;ListTargetConst&gt; ::= u </summary>
		ListTargetConst_u          = 168,    
		/// <summary> &lt;ListTargetConst&gt; ::= users </summary>
		ListTargetConst_users      = 169,    
		/// <summary> &lt;ListTargetConst&gt; ::= ob </summary>
		ListTargetConst_ob         = 170,    
		/// <summary> &lt;ListTargetConst&gt; ::= objects </summary>
		ListTargetConst_objects    = 171,    
		/// <summary> &lt;ListDetailConst&gt; ::= d </summary>
		ListDetailConst_d          = 172,    
		/// <summary> &lt;ListDetailConst&gt; ::= detailed </summary>
		ListDetailConst_detailed   = 173,    
		/// <summary> &lt;ListConst&gt; ::= &lt;ListListConst&gt; </summary>
		ListConst                  = 174,    
		/// <summary> &lt;ListConst&gt; ::= &lt;ListTargetConst&gt; </summary>
		ListConst2                 = 175,    
		/// <summary> &lt;ListConst&gt; ::= &lt;ListDetailConst&gt; </summary>
		ListConst3                 = 176,    
		/// <summary> &lt;AdminConst&gt; ::= globals </summary>
		AdminConst_globals         = 177,    
		/// <summary> &lt;AdminConst&gt; ::= gl </summary>
		AdminConst_gl              = 178,    
		/// <summary> &lt;AdminConst&gt; ::= connections </summary>
		AdminConst_connections     = 179,    
		/// <summary> &lt;AdminConst&gt; ::= cn </summary>
		AdminConst_cn              = 180,    
		/// <summary> &lt;AdminConst&gt; ::= clients </summary>
		AdminConst_clients         = 181,    
		/// <summary> &lt;AdminConst&gt; ::= cl </summary>
		AdminConst_cl              = 182,    
		/// <summary> &lt;AdminConst&gt; ::= last </summary>
		AdminConst_last            = 183,    
		/// <summary> &lt;AdminConst&gt; ::= l </summary>
		AdminConst_l               = 184,    
		/// <summary> &lt;AdminConst&gt; ::= cut </summary>
		AdminConst_cut             = 185,    
		/// <summary> &lt;AdminConst&gt; ::= status </summary>
		AdminConst_status          = 186,    
		/// <summary> &lt;AdminConst&gt; ::= st </summary>
		AdminConst_st              = 187,    
		/// <summary> &lt;AdminConst&gt; ::= reset </summary>
		AdminConst_reset           = 188,    
		/// <summary> &lt;AdminConst&gt; ::= r </summary>
		AdminConst_r               = 189,    
		/// <summary> &lt;AdminConst&gt; ::= data </summary>
		AdminConst_data            = 190,    
		/// <summary> &lt;Value&gt; ::= &lt;StandardValue&gt; </summary>
		Value                      = 191,    
		/// <summary> &lt;Value&gt; ::= &lt;CommandConst&gt; </summary>
		Value2                     = 192,    
		/// <summary> &lt;Value&gt; ::= &lt;CheckConst&gt; </summary>
		Value3                     = 193,    
		/// <summary> &lt;Value&gt; ::= on </summary>
		Value_on                   = 194,    
		/// <summary> &lt;Value&gt; ::= off </summary>
		Value_off                  = 195,    
		/// <summary> &lt;Value&gt; ::= &lt;ListConst&gt; </summary>
		Value4                     = 196,    
		/// <summary> &lt;Value&gt; ::= &lt;AdminConst&gt; </summary>
		Value5                     = 197,    
		/// <summary> &lt;CheckValue&gt; ::= &lt;StandardValue&gt; </summary>
		CheckValue                 = 198,    
		/// <summary> &lt;CheckValue&gt; ::= &lt;CommandConst&gt; </summary>
		CheckValue2                = 199,    
		/// <summary> &lt;CheckValue&gt; ::= on </summary>
		CheckValue_on              = 200,    
		/// <summary> &lt;CheckValue&gt; ::= off </summary>
		CheckValue_off             = 201,    
		/// <summary> &lt;CheckValue&gt; ::= &lt;ListConst&gt; </summary>
		CheckValue3                = 202,    
		/// <summary> &lt;ListSelectValue&gt; ::= &lt;StandardValue&gt; </summary>
		ListSelectValue            = 203,    
		/// <summary> &lt;ListSelectValue&gt; ::= &lt;CommandConst&gt; </summary>
		ListSelectValue2           = 204,    
		/// <summary> &lt;ListSelectValue&gt; ::= &lt;CheckConst&gt; </summary>
		ListSelectValue3           = 205,    
		/// <summary> &lt;ListSelectValue&gt; ::= on </summary>
		ListSelectValue_on         = 206,    
		/// <summary> &lt;ListSelectValue&gt; ::= off </summary>
		ListSelectValue_off        = 207,    
		/// <summary> &lt;ListSelectValue&gt; ::= &lt;AdminConst&gt; </summary>
		ListSelectValue4           = 208,    
		/// <summary> &lt;LogValue&gt; ::= NormalConst </summary>
		LogValue_NormalConst       = 209,    
		/// <summary> &lt;LogValue&gt; ::= StringConst </summary>
		LogValue_StringConst       = 210,    
		/// <summary> &lt;LogValue&gt; ::= EscapedConst </summary>
		LogValue_EscapedConst      = 211,    
		/// <summary> &lt;LogValue&gt; ::= s </summary>
		LogValue_s                 = 212,    
		/// <summary> &lt;LogValue&gt; ::= f </summary>
		LogValue_f                 = 213     
	}
	
	public class CommandLineNode : SyntaxNode
	{
		public CommandLineNode(Rule rule, SyntaxNode[] nodes) : base(rule, nodes) {}
		
		public CommandLineNode(Symbol symbol, string content) : base(symbol, content) {}
		
		public new CommandLineSymbols SymbolIndex
		{
			get
			{
				return (CommandLineSymbols) base.SymbolIndex;
			}
		}
		
		public new CommandLineRules RuleIndex
		{
			get
			{
				return (CommandLineRules) base.RuleIndex;
			}
		}
		
		public new CommandLineNode this[int index]
		{
			get
			{
				return (CommandLineNode) base[index];
			}
		}
		
		public new CommandLineNode Trimmed
		{
			get
			{
				return (CommandLineNode) base.Trimmed;
			}
		}
	}
}
