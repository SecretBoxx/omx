using System;

namespace Orbiter.Multiplayer
{
    public abstract class GlobalObject
    {
        private UInt16 globalId = UInt16.MaxValue;
        private Server server;
        private string name;

        public UInt16 GlobalId
        {
            get { return globalId; }
            set { globalId = value; }
        }

        protected Server Server
        {
            get { return server; }
        }

        public GlobalObject(Server server)
        {
            this.server = server;
        }

        public string Name
        {
            get { return name; }
            protected set { name = value; }
        }

        ~GlobalObject()
        {
            server.Map.ReleaseGlobalId(globalId, GetHashCode());
        }

        public abstract bool EvaluateInfo(params string[] strings);
        public abstract string[] GenerateInfo();
    }
}
