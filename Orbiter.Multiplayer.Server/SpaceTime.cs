namespace Orbiter.Multiplayer
{
    public class SpaceTime
    {
        private Time time;
        private CelestialBody space;

        public SpaceTime(Time time, CelestialBody space)
        {
            this.time = time;
            this.space = space;
        }
    }
}
