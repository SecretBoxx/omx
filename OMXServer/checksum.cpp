#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include "checksum.h"

/* --------------------------------------------------------------------
* main() is a simple routine (not included in DLL) to generate or check
* for passwords. It provides an example to call DLL's members.
* Author: Boxx with ChatGPT-assistance, let all devs who trained ChatGPT be thanked.
* (c) 2023.
*/

int main() {
    std::string username, password;
    std::cout << "Enter username: "; std::getline(std::cin, username);
    std::cout << "Enter password: "; std::getline(std::cin, password);

    produceChecksumLine(username, password);

    std::ifstream infile("passChecking_new");
    std::string line;
    bool authenticated = false;
    while (std::getline(infile, line)) {
        std::string stored_username, stored_checksum;
        std::stringstream ss(line);
        std::getline(ss, stored_username, ',');
        std::getline(ss, stored_checksum);

        if (username == stored_username) {
            std::cout << "(user found...) ";
            std::string computed_checksum = calculateChecksum(password);
            if (computed_checksum == stored_checksum) {
                std::cout << "Authentication successful." << std::endl;
                authenticated = true;
                break;
            }
        }
        std::cout << std::endl;
    }

    if (!authenticated) {
        std::cout << "Authentication failed." << std::endl;
    }

    return 0;
}
/* --------------------------------------------------------------------*/

std::string calculateChecksum(std::string password) {
    uint16_t checksum = 0;
    for (char c : password) {
        checksum += (uint16_t)c;
        checksum = (checksum << 1) | (checksum >> 15);
    }
    std::stringstream ss;
    ss << std::hex << std::setw(4) << std::setfill('0') << checksum;
    return ss.str();
}


bool produceChecksumLine(std::string username, std::string password) {
    // Open the output file that will contain the username and corresponding checksum
    std::ofstream outputFile("passwordChecking_new", std::ofstream::app);
    if (!outputFile) {
        std::cerr << "Error: Unable to open output file." << std::endl;
        return 1;
    }

    std::string checksum = calculateChecksum(password);
    // Write the username and checksum to the output file
    outputFile << username << "," << checksum << std::endl;

    outputFile.close();
    return 0;
}

