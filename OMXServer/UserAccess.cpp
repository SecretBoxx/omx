/*
#include <cstdint>
*/
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include "UserAccess.h"
#include "checksum.h"


int bidule(int a, int b) {
    int sum = 0;
    std::cout << "Entering in bidule\n";
    sum = a + b + 1;
    std::cout << "My personal sum of " << a << " and " << b << " is " << sum << "\n";
    return sum;
}

/// <summary>
/// Searches for username in the file PASSWORDCHECKSUM and compares the checksum of the password with the stroed checksum, i.e. no password is stored in PASSWORDCHECKSUM. Pointers must be passed to the routine.
/// </summary>
/// <param name="usernameLength">Length, from C#: usernameBytes.Length, after byte[] usernameBytes = System.Text.Encoding.UTF8.GetBytes(user);</param>
/// <param name="usernamePtr">Pointer to the string username, from C#: IntPtr usernamePtr = Marshal.AllocHGlobal(usernameBytes.Length); Marshal.Copy(usernameBytes, 0, usernamePtr, usernameBytes.Length);(</param>
/// <param name="passwordLength">Length, from C#: passwordBytes.Length, after byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);</param>
/// <param name="passwordPtr">Pointer to the string password, from C#: IntPtr passwordPtr = Marshal.AllocHGlobal(passwordBytes.Length); Marshal.Copy(passwordBytes, 0, passwordPtr, passwordBytes.Length);</param>
/// <returns>-1 or 1, if user known and password wrong or true, or 0 if user unknown</returns>
/// <notes> Don't forget to free the pinned memory: Marshal.FreeHGlobal(usernamePtr); Marshal.FreeHGlobal(passwordPtr);</notes>
int checkPassword(char* usernamePtr, int usernameLength, char* passwordPtr, int passwordLength) {
    std::string stored_username, stored_checksum, stored_email;
    std::string line;
    
    // Access the referenced values of username and password
    std::string username(usernamePtr, usernameLength);
    std::string password(passwordPtr, passwordLength);
    //std::cout << "Username: " << username << std::endl;
    //std::cout << "Password: " << password << std::endl;

    // if there is a need to modify the values of username and password
    //std::string newUsername("newUsername");
    //std::string newPassword("newPassword");
    //memcpy(usernamePtr, newUsername.c_str(), newUsername.length());
    //memcpy(passwordPtr, newPassword.c_str(), newPassword.length());

    std::ifstream infile(PASSWORDCHECKSUM);
    while (std::getline(infile, line)) {
        std::stringstream ss(line);
        std::getline(ss, stored_username, ',');
        if (username == stored_username) {
            std::getline(ss, stored_email, ',');
            std::getline(ss, stored_checksum);
            std::string computed_checksum = calculateChecksum(password);
            //std::cout << "computed_checksum: " << computed_checksum;
            //std::cout << " = stored_checksum: " << stored_checksum << std::endl;
            if (computed_checksum == stored_checksum) {
                return 1;
            }
            else return -1;
        }
    }
    return 0;
}
