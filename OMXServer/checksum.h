#ifndef CHECKSUM_H
#define CHECKSUM_H

/// <summary>
/// computes a string, called checksum, from the processing of a password.
/// </summary>
/// <param name="password"></param>
/// <returns>checksum</returns>
std::string calculateChecksum(std::string password);

/// <summary>
/// produces the line "username,checksum" that should be added to the file that
/// contains all user access checksums.
/// </summary>
/// <param name="username"></param>
/// <param name="password"></param>
/// <returns>local file "passwordChecking_new" with a new line added</returns>
bool produceChecksumLine(std::string username, std::string password);

/* a good website to generate passwords can be this one:
* https://passwordsgenerator.net
* with these options:
* https://passwordsgenerator.net/?length=16&symbols=0&numbers=1&lowercase=1&uppercase=1&similar=1&ambiguous=0&client=1&autoselect=1
* Also for name generators, this great website:
* https://www.fantasynamegenerators.com/spaceship-names.php
*/

/*
* How to use it as a library file *
*1* Compile the checksum.cpp file into a library file (e.g., checksum.lib on Windows
	or libchecksum.a on Linux) using the appropriate compiler and linker commands
	(with MinGW GCC):
		g++ -c checksum.cpp -o checksum.o
		ar rcs libchecksum.a checksum.o
*2* In the main program, include the checksum.h header file to declare the
	calculateChecksum() function. This will generate a static library file called 
	libchecksum.a
*3* Link the main program with the checksum.lib or libchecksum.a library file to
	enable the main program to call the calculateChecksum() function:
		g++ main.cpp -L. -lchecksum -o program.exe
*/
#endif  // CHECKSUM_H
